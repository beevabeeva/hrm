import os
import subprocess

def run_r_script(script_path):
    """
    Executes an R script on the host machine.


    Args:
        script_path (str): Path to the R script.
        run_all_timetable_for_classes.R [gets all dataframes ans saves them as rds files to some specified dir that must be also mounted to the container]


    """
    if not os.path.isfile(script_path):
        raise FileNotFoundError(f"The file '{script_path}' does not exist.")

    command = ["Rscript", script_path]
    print(f"Running R script: {script_path}...")
    subprocess.run(command, check=True)

def start_container_with_mounts(container_name, local_src_dir, container_src_dir, local_rds_dir, container_rds_dir):
    """
    Starts a Docker container with multiple directories mounted.

    Args:
        container_name (str): Name of the Docker container.
        local_src_dir (str): Path to the local source directory to mount.
        container_src_dir (str): Path inside the container where the source directory will be mounted.
        local_rds_dir (str): Path to the local RDS directory to mount.
        container_rds_dir (str): Path inside the container where the RDS directory will be mounted.
    """
    if not os.path.isdir(local_src_dir):
        raise NotADirectoryError(f"The directory '{local_src_dir}' does not exist.")
    if not os.path.isdir(local_rds_dir):
        raise NotADirectoryError(f"The directory '{local_rds_dir}' does not exist.")

    command = [
        "docker", "run", "-d",
        "--name", container_name,
        "-v", f"{local_src_dir}:{container_src_dir}",
        "-v", f"{local_rds_dir}:{container_rds_dir}",
        "gurobi_image",  # Replace with your Docker image name
        "tail", "-f", "/dev/null"  # Keeps the container running
    ]
    print(f"Starting container {container_name} with mounts:\n"
          f"  - {local_src_dir} -> {container_src_dir}\n"
          f"  - {local_rds_dir} -> {container_rds_dir}")
    subprocess.run(command, check=True)

def exec_r_script_in_container(container_name, container_r_script_path):
    """
    Executes an R script inside the Docker container.

    Args:
        container_name (str): Name of the Docker container.
        container_r_script_path (str): Path to the R script inside the container.
       run_timetable_for_classes (reads in rds files [in the container now] calls reticulate, calls timetable_pulp_no_class_ass.py [mounted])

    """
    command = ["docker", "exec", container_name, "Rscript", container_r_script_path]
    print(f"Executing R script {container_r_script_path} in container {container_name}...")
    subprocess.run(command, check=True)

def copy_output_from_container(container_name, container_output_dir, local_output_dir):
    """
    Copies output files from the Docker container to the local machine.

    Args:
        container_name (str): Name of the Docker container.
        container_output_dir (str): Directory inside the container with output files.
        local_output_dir (str): Destination directory on the host machine.
    """
    if not os.path.isdir(local_output_dir):
        os.makedirs(local_output_dir)

    command = ["docker", "cp", f"{container_name}:{container_output_dir}/.", local_output_dir]
    print(f"Copying output files from {container_output_dir} in container {container_name} to {local_output_dir}...")
    subprocess.run(command, check=True)

def stop_and_remove_container(container_name):
    """
    Stops and removes the Docker container.

    Args:
        container_name (str): Name of the Docker container.
    """
    print(f"Stopping and removing container {container_name}...")
    subprocess.run(["docker", "stop", container_name], check=True)
    subprocess.run(["docker", "rm", container_name], check=True)

def main():
    # Configuration
    r_script_host = "path/to/your/get_data.R"  # Host R script to fetch data
    container_name = "gurobi_container"  # Name of your Docker container
    local_src_dir = "path/to/your/source/"  # Local directory with scripts
    container_src_dir = "/app"  # Path inside the container
    container_r_script = "/app/process_data.R"  # R script inside the container
    local_rds_dir = "path/to/your/rds/"  # Local directory with RDS files
    container_rds_dir = "/app/data"  # Path inside the container for RDS files
    container_output_dir = "/app/output"  # Output directory inside the container
    local_output_dir = "path/to/your/output/"  # Local directory for output files

    try:
        # Step 1: Run R script on the host to generate RDS files
        run_r_script(r_script_host)

        # Step 2: Start the Docker container with the source directory mounted
        start_container_with_mount(container_name, local_src_dir, container_src_dir)

        # Step 3: Execute the R script inside the container
        exec_r_script_in_container(container_name, container_r_script)

        # Step 4: Copy output files from the container to the host
        copy_output_from_container(container_name, container_output_dir, local_output_dir)

    finally:
        # Step 5: Stop and remove the container
        stop_and_remove_container(container_name)

if __name__ == "__main__":
    main()
