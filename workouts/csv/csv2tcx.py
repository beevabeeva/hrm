import csv
import xml.etree.ElementTree as ET
from datetime import datetime

def convert_csv_to_tcx(csv_file, tcx_file):
    # Create the root <TrainingCenterDatabase> element
    tcx = ET.Element("TrainingCenterDatabase", xmlns="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", xsi="http://www.w3.org/2001/XMLSchema-instance")
    
    # Create the <Activities> and <Activity> elements
    activities = ET.SubElement(tcx, "Activities")
    activity = ET.SubElement(activities, "Activity", Sport="Biking")
    
    # Set activity ID (start time of activity)
    activity_start_time = "2024-12-10T14:29:12Z"
    activity_id = ET.SubElement(activity, "Id")
    activity_id.text = activity_start_time

    # Add creator information
    creator = ET.SubElement(activity, "Creator", xsi_type="Device_t")
    name = ET.SubElement(creator, "Name")
    name.text = "Garmin TCX with Barometer"
    unit_id = ET.SubElement(creator, "UnitId")
    unit_id.text = "0"
    product_id = ET.SubElement(creator, "ProductId")
    product_id.text = "20119"
    version = ET.SubElement(creator, "Version")
    version_major = ET.SubElement(version, "VersionMajor")
    version_major.text = "0"
    version_minor = ET.SubElement(version, "VersionMinor")
    version_minor.text = "0"
    build_major = ET.SubElement(version, "BuildMajor")
    build_major.text = "0"
    build_minor = ET.SubElement(version, "BuildMinor")
    build_minor.text = "0"

    # Add Lap element with start time
    lap_start_time = "2024-12-10T14:29:12Z"
    lap = ET.SubElement(activity, "Lap", StartTime=lap_start_time)
    
    # Add lap data
    total_time_seconds = 4379
    distance_meters = 30091.6
    maximum_speed = 35
    calories = 366
    avg_heart_rate_bpm = 134
    max_heart_rate_bpm = 154
    intensity = "Active"
    trigger_method = "Manual"
    
    total_time = ET.SubElement(lap, "TotalTimeSeconds")
    total_time.text = str(total_time_seconds)
    distance = ET.SubElement(lap, "DistanceMeters")
    distance.text = str(distance_meters)
    max_speed = ET.SubElement(lap, "MaximumSpeed")
    max_speed.text = str(maximum_speed)
    lap_calories = ET.SubElement(lap, "Calories")
    lap_calories.text = str(calories)
    
    avg_hr = ET.SubElement(lap, "AverageHeartRateBpm")
    avg_hr_value = ET.SubElement(avg_hr, "Value")
    avg_hr_value.text = str(avg_heart_rate_bpm)
    
    max_hr = ET.SubElement(lap, "MaximumHeartRateBpm")
    max_hr_value = ET.SubElement(max_hr, "Value")
    max_hr_value.text = str(max_heart_rate_bpm)
    
    lap_intensity = ET.SubElement(lap, "Intensity")
    lap_intensity.text = intensity
    
    trigger = ET.SubElement(lap, "TriggerMethod")
    trigger.text = trigger_method
    
    # Create the <Track> element for the lap
    track = ET.SubElement(lap, "Track")

    # Open the CSV file and process the data
    with open(csv_file, 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            timestamp = datetime.strptime(row['timestamp'], "%Y-%m-%d %H:%M:%S")
            trackpoint = ET.SubElement(track, "Trackpoint")
            time = ET.SubElement(trackpoint, "Time")
            time.text = timestamp.isoformat() + "Z"  # Format timestamp as ISO 8601
            
            # Add distance
            distance_meters = row['distance']
            distance = ET.SubElement(trackpoint, "DistanceMeters")
            distance.text = str(distance_meters)
            
            # Add heart rate
            heart_rate_bpm = row['heart_rate']
            heart_rate = ET.SubElement(trackpoint, "HeartRateBpm", xsi_type="HeartRateInBeatsPerMinute_t")
            heart_rate_value = ET.SubElement(heart_rate, "Value")
            heart_rate_value.text = heart_rate_bpm
            
            # Add cadence
            cadence = row['cadence']
            cadence_element = ET.SubElement(trackpoint, "Cadence")
            cadence_element.text = cadence
            
            # Add Extensions (Speed and Watts)
            extensions = ET.SubElement(trackpoint, "Extensions")
            tpx = ET.SubElement(extensions, "TPX", xmlns="http://www.garmin.com/xmlschemas/ActivityExtension/v2")
            speed = row['speed']
            speed_element = ET.SubElement(tpx, "Speed")
            speed_element.text = speed
            watts = row['power']
            watts_element = ET.SubElement(tpx, "Watts")
            watts_element.text = watts

    # Write the TCX file
    tree = ET.ElementTree(tcx)
    tree.write(tcx_file)

# Example usage
convert_csv_to_tcx('workouts/workout_20241210_162912.csv', 'output_activity.tcx')
