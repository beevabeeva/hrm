import os
import time
import json
import requests
from datetime import datetime

# File to store auth tokens and client info
AUTH_FILE = 'strava.auth'

# Strava API endpoints
TOKEN_URL = 'https://www.strava.com/api/v3/oauth/token'

def read_auth_file():
    """Read the auth file and return a dictionary of tokens and client info."""
    auth_data = {}
    if os.path.exists(AUTH_FILE):
        with open(AUTH_FILE, 'r') as f:
            for line in f:
                key, value = line.strip().split('=')
                auth_data[key] = value
    return auth_data

def write_auth_file(auth_data):
    """Write the auth data to the auth file."""
    with open(AUTH_FILE, 'w') as f:
        for key, value in auth_data.items():
            f.write(f"{key}={value}\n")

def get_new_access_token(refresh_token, client_id, client_secret):
    """Get a new access token using the refresh token."""
    response = requests.post(
        TOKEN_URL,
        data={
            'client_id': client_id,
            'client_secret': client_secret,
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token
        }
    )
    if response.status_code != 200:
        raise Exception("Failed to refresh token")
    return response.json()

def strava_tokens():
    # Read current auth data
    auth_data = read_auth_file()
    
    # Check if we have all the necessary information
    required_keys = ['access_token', 'refresh_token', 'client_id', 'client_secret']
    if not all(key in auth_data for key in required_keys):
        raise Exception("Missing required information in auth file")

    # Check if the access token has expired
    current_time = int(time.time())
    expires_at = int(auth_data.get('expires_at', 0))

    if expires_at <= current_time:
        print("Access token has expired. Refreshing...")
        # Get new tokens
        new_tokens = get_new_access_token(
            auth_data['refresh_token'],
            auth_data['client_id'],
            auth_data['client_secret']
        )
        
        # Update tokens
        auth_data['access_token'] = new_tokens['access_token']
        auth_data['refresh_token'] = new_tokens['refresh_token']
        auth_data['expires_at'] = str(new_tokens['expires_at'])
        auth_data['expires_in'] = str(new_tokens['expires_in'])
        
        # Save new tokens to file
        write_auth_file(auth_data)
        print("New access token obtained and saved.")
    else:
        print("Using existing short-lived access token.")

    print(f"Current access token: {auth_data['access_token']}")
    print(f"Expires at: {datetime.fromtimestamp(int(auth_data['expires_at']))}")

# if __name__ == "__main__": # comment out for combining in other file (uploader.py)
#     strava_tokens()