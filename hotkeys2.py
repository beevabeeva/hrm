import tkinter as tk
from tkinter import messagebox
from Cocoa import NSApp, NSWindow

# Import the specific style mask constants
NSBorderlessWindowMask = 0x00020000
NSTitledWindowMask = 0x00000000

class CustomWindow(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Custom Window")
        self.geometry("800x600")
        self.configure(bg='lightgrey')

        # Remove window decorations
        self.remove_decorations()

        # Button to show/hide window
        self.button = tk.Button(self, text="Hide/Show", command=self.toggle_window)
        self.button.pack()

        # Bind to <Command-S> for saving
        self.bind_all('<Command-s>', self.save_current_file)

    def remove_decorations(self):
        window = NSApp().windows()[0]
        style_mask = window.styleMask()
        # Remove the title bar
        new_style_mask = style_mask & ~NSTitledWindowMask
        window.setStyleMask_(new_style_mask)

    def toggle_window(self):
        if self.state() == 'normal':
            self.withdraw()
        else:
            self.deiconify()

    def save_current_file(self, event):
        messagebox.showinfo("Save", "File saved! (fake)")

if __name__ == "__main__":
    app = CustomWindow()
    app.mainloop()
