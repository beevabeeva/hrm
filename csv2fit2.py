import csv
import datetime
import struct
from garmin_fit_sdk import Stream, Decoder, Profile

def calculate_crc(data):
    crc_table = [
        0x0000, 0xCC01, 0xD801, 0x1400, 0xF001, 0x3C00, 0x2800, 0xE401,
        0xA001, 0x6C00, 0x7800, 0xB401, 0x5000, 0x9C01, 0x8801, 0x4400
    ]
    
    crc = 0
    for byte in data:
        # compute checksum of lower four bits of byte
        tmp = crc_table[crc & 0xF]
        crc = (crc >> 4) & 0x0FFF
        crc = crc ^ tmp ^ crc_table[byte & 0xF]
        
        # compute checksum of upper four bits of byte
        tmp = crc_table[crc & 0xF]
        crc = (crc >> 4) & 0x0FFF
        crc = crc ^ tmp ^ crc_table[(byte >> 4) & 0xF]
    
    return crc

def create_fit_header(data_size):
    header_size = 14
    protocol_version = 0x20  # Protocol version 2.0
    profile_version = 2100   # Profile version 21.00
    
    # Create header bytes without CRC
    header = struct.pack('<BBHI4s', 
        header_size,        # Header size
        protocol_version,   # Protocol version
        profile_version,    # Profile version
        data_size,         # Data size
        b'.FIT'            # .FIT magic string
    )
    
    # Calculate CRC
    crc = calculate_crc(header)
    
    # Add CRC to header
    return header + struct.pack('<H', crc)

def encode_message(msg_type, msg_data):
    # Basic message encoding - this is a simplified version
    # In a real implementation, you'd need to handle all field types properly
    encoded = bytearray()
    if msg_type == "file_id":
        # Local message type 0
        encoded.extend([0x40, 0x00])  # Definition message
        encoded.extend([0x00, 0x00])  # Data message
    elif msg_type == "record":
        # Local message type 1
        encoded.extend([0x40, 0x01])  # Definition message
        encoded.extend([0x01, 0x00])  # Data message
    return encoded

def convert_csv_to_fit(csv_file, fit_file):
    messages = []
    
    # Read CSV and prepare messages
    with open(csv_file, 'r') as file:
        reader = csv.DictReader(file)
        
        # Add File ID Message
        file_id_msg = {
            "type": "activity",
            "manufacturer": "development",
            "product": 0,
            "serial_number": 0x12345678,
            "time_created": int(datetime.datetime.now().timestamp())
        }
        messages.append(("file_id", file_id_msg))
        
        # Add record messages
        for row in reader:
            timestamp = datetime.datetime.strptime(row['timestamp'], '%Y-%m-%d %H:%M:%S')
            timestamp_seconds = int(timestamp.timestamp())
            
            record_msg = {
                "timestamp": timestamp_seconds,
                "heart_rate": int(row['heart_rate']),
                "power": int(row['power']),
                "cadence": int(row['cadence']),
                "speed": float(row['speed']),
                "distance": float(row['distance'])
            }
            messages.append(("record", record_msg))

    # Encode messages and calculate total size
    encoded_data = bytearray()
    for msg_type, msg_data in messages:
        encoded_msg = encode_message(msg_type, msg_data)
        encoded_data.extend(encoded_msg)

    data_size = len(encoded_data)
    
    # Create the FIT file
    with open(fit_file, 'wb') as outfile:
        # Write header
        header = create_fit_header(data_size)
        outfile.write(header)
        
        # Write encoded messages
        outfile.write(encoded_data)
        
        # Write CRC for entire file
        file_crc = calculate_crc(header + encoded_data)
        outfile.write(struct.pack('<H', file_crc))

    # Verify the created file
    try:
        stream = Stream.from_file(fit_file)
        decoder = Decoder(stream)
        
        if decoder.is_fit():
            messages, errors = decoder.read()
            if errors:
                print(f"Warning: File created with errors: {errors}")
            else:
                print("FIT file created and verified successfully")
        else:
            print("Error: Created file is not a valid FIT file")
    except Exception as e:
        print(f"Error verifying file: {str(e)}")


# convert_csv_to_fit('workouts/csv/workout_20241013_081722.csv', 'test2.fit')
convert_csv_to_fit('propercsv.csv', 'test2.fit')