import requests
import time

def strava_upload(file_path, callback,progress_bar):

    # Read the tokens from the .auth file
    tokens = {}
    with open('strava.auth', 'r') as auth_file:
        for line in auth_file:
            key, value = line.strip().split('=')
            tokens[key] = value

    access_token = tokens.get('access_token')
    refresh_token = tokens.get('refresh_token')

    # Define the URL for the Strava API endpoint
    url = 'https://www.strava.com/api/v3/uploads'

    # Define the headers
    headers = {
        'Authorization': f'Bearer {access_token}',
        'Accept': 'application/json'
    }

    # Define the data to be sent in the request
    data = {
        'name': '', # Maybe add option to edit this later
        'description': '', # Maybe add option to edit this later
        'data_type': 'tcx',
        'external_id': file_path
    }

    # Define the file to be uploaded
    files = {
        'file': (file_path, open(file_path, 'rb'), 'application/octet-stream')
    }

    # Send the POST request
    response = requests.post(url, headers=headers, data=data, files=files)
    
    # Send the response status to the label
    callback(f"Upload response status: {response.status_code}",progress_bar)
    print(f"Upload response status: {response.status_code}")

    response_json = response.json()
    upload_id = response_json.get('id')
    callback(f"Upload ID: {upload_id}",progress_bar)
    print(f"Upload ID: {upload_id}")

    if response.status_code == 201 and upload_id:
        status_url = f'https://www.strava.com/api/v3/uploads/{upload_id}'
        attempts = 5
        while attempts >= 0:
            status_response = requests.get(status_url, headers=headers)
            status_json = status_response.json()
            print(status_json) #Debug
            upload_status = status_json.get('status')
            error_status = status_json.get('error')
            print(f"Error message: {error_status}")
            # Send the upload status to the label
            callback(f"Upload status: {upload_status}",progress_bar)
            print(f"Upload status: {upload_status}")

            # if ! "None" in error_status:
            if error_status is not None:
                print(f"Error message: {error_status}")
                callback(f"{error_status}",progress_bar)
                
            if upload_status == "Your activity is ready.":
                callback("Activity is ready!",progress_bar)
                print("Activity is ready!")
                break
            elif upload_status == "There was an error processing your activity.":
                callback(f"Error processing activity. Failed to upload the file.\n {error_status}",progress_bar)
                print("Error processing activity. Failed to upload the file.")
                break

            attempts -= 1
            time.sleep(2)
    else:
        callback("Failed to upload the file.",progress_bar)
        print("Failed to upload the file.")