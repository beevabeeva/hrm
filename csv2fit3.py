import csv
import os
import sys
import struct
import datetime
from typing import List, Dict, Any

# Requires installation of fitparse library
# pip install fitparse pandas

import fitparse

class FITWriter:
    def __init__(self, output_filename: str):
        """
        Initialize FIT file writer
        
        Args:
            output_filename (str): Path to the output .fit file
        """
        self.output_filename = output_filename
        self.fit_file = open(output_filename, 'wb')
        self.header_written = False
        
        # FIT file constants
        self.HEADER_SIZE = 14
        self.PROTOCOL_VERSION = 0x10
        self.PROFILE_VERSION = 0x0803  # FIT SDK version
        
    def write_header(self, data_size: int):
        """
        Write FIT file header
        
        Args:
            data_size (int): Size of the data to be written
        """
        # header_bytes = struct.pack('<BHBBBBBL',
        #     self.HEADER_SIZE,       # Byte 0: Header size
        #     data_size,              # Bytes 1-2: Data size (little-endian)
        #     0x2E, 0x46, 0x49, 0x54, # File type ".FIT"
        #     self.PROTOCOL_VERSION,  # Protocol version
        #     self.PROFILE_VERSION,   # Profile version
        #     0xFFFFFFFF              # CRC not calculated yet
        # )
    #     header_bytes = struct.pack('<BLBBBBL',
    #     self.HEADER_SIZE,       # Byte 0: Header size
    #     data_size,              # Bytes 1-4: Data size (little-endian)
    #     0x2E, 0x46, 0x49, 0x54,        # File type ".FIT" as 2 separate 16-bit values
    #     self.PROTOCOL_VERSION,  # Protocol version (1 byte)
    #     self.PROFILE_VERSION,    # Profile version (1 byte)
    #     0xFFFFFFFF              # CRC not calculated yet
    # )
        header_bytes = struct.pack('<BBHL4s',
            self.HEADER_SIZE,       # Byte 0: Header size (1 byte)
            self.PROTOCOL_VERSION,  # Byte 1: Protocol version (1 byte)
            self.PROFILE_VERSION,   # Bytes 2-3: Profile version (2 bytes)
            data_size,              # Bytes 4-7: Data size (4 bytes)
            b'.FIT'                 # Bytes 8-11: File type ".FIT" (4 bytes)
        )
        self.fit_file.write(header_bytes)
        self.header_written = True
        
    def calculate_crc(self, data: bytes) -> int:
        """
        Calculate CRC-16 for FIT file
        
        Args:
            data (bytes): Data to calculate CRC for
        
        Returns:
            int: Calculated CRC value
        """
        crc = 0
        for byte in data:
            crc ^= byte
            for _ in range(8):
                if crc & 0x01:
                    crc = (crc >> 1) ^ 0x8408
                else:
                    crc >>= 1
        return crc
    
    def write_data_records(self, records: List[Dict[str, Any]], message_type: int = 21):
        """
        Write FIT data records
        
        Args:
            records (List[Dict]): List of record dictionaries
            message_type (int): FIT message type (defaults to record type)
        """
        if not self.header_written:
            raise ValueError("Header must be written before data records")
        
        def get_field_definition(field_name: str, field_type: str) -> tuple:
            """
            Determine field definition and conversion
            
            Args:
                field_name (str): Name of the field
                field_type (str): Type of the field
            
            Returns:
                tuple: (field number, size, base type)
            """
            field_type_map = {
                'int': (0x83, 4),   # signed 32-bit integer
                'float': (0x84, 4),  # float
                'string': (0x07, 255),  # string
                'datetime': (0x86, 4),  # timestamp
            }
            
            default_type = 'int'
            type_key = field_type if field_type in field_type_map else default_type
            field_num = list(records[0].keys()).index(field_name)
            field_base_type = field_type_map.get(type_key, field_type_map[default_type])
            
            return (field_num, field_base_type[1], field_base_type[0])
        
        # Create field definitions
        first_record = records[0]
        definitions = [get_field_definition(field, str(type(first_record[field]).__name__)) 
                       for field in first_record.keys()]
        
        data_to_write = bytearray()
        
        # Write records
        for record in records:
            # Definition message
            def_msg = bytearray([
                0x40 | message_type,  # Definition message header
                0x00,  # Reserved
                0x00,  # Architecture (little-endian)
                len(definitions)  # Number of fields
            ])
            
            for field_def in definitions:
                def_msg.extend(struct.pack('<BBB', *field_def))
            
            # Data message
            data_msg = bytearray([
                0x00 | message_type,  # Data message header
            ])
            
            for field in first_record.keys():
                value = record[field]
                
                # Convert different types
                if isinstance(value, int):
                    data_msg.extend(struct.pack('<i', value))
                elif isinstance(value, float):
                    data_msg.extend(struct.pack('<f', value))
                elif isinstance(value, str):
                    data_msg.extend(value.encode('utf-8') + b'\x00')
                elif isinstance(value, datetime.datetime):
                    # FIT timestamp is seconds since UTC 1989-12-31
                    timestamp = int((value - datetime.datetime(1989, 12, 31)).total_seconds())
                    data_msg.extend(struct.pack('<I', timestamp))
            
            data_to_write.extend(def_msg)
            data_to_write.extend(data_msg)
        
        self.fit_file.write(data_to_write)
    
    def close(self):
        """Close the FIT file and finalize"""
        # Go back and update header with actual data size
        self.fit_file.seek(1)
        file_size = os.path.getsize(self.output_filename)
        self.fit_file.write(struct.pack('<H', file_size - self.HEADER_SIZE))
        
        # Calculate and write CRC
        self.fit_file.seek(0)
        file_data = self.fit_file.read()
        crc = self.calculate_crc(file_data[:-2])
        
        self.fit_file.seek(file_size - 2)
        self.fit_file.write(struct.pack('<H', crc))
        
        self.fit_file.close()

def convert_csv_to_fit(input_csv: str, output_fit: str):
    """
    Convert CSV file to FIT file
    
    Args:
        input_csv (str): Path to input CSV file
        output_fit (str): Path to output FIT file
    """
    # Read CSV file
    with open(input_csv, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        records = list(reader)
    
    # Convert string values to appropriate types
    converted_records = []
    for record in records:
        converted_record = {}
        for key, value in record.items():
            try:
                # Try converting to appropriate type
                if '.' in value:
                    converted_record[key] = float(value)
                else:
                    converted_record[key] = int(value)
            except ValueError:
                # Keep as string if conversion fails
                converted_record[key] = value
        converted_records.append(converted_record)
    
    # Write FIT file
    fit_writer = FITWriter(output_fit)
    fit_writer.write_header(len(converted_records) * 100)  # Estimated size
    fit_writer.write_data_records(converted_records)
    fit_writer.close()
    
    print(f"Converted {input_csv} to {output_fit}")

def main():
    if len(sys.argv) != 3:
        print("Usage: python csv2fit.py input.csv output.fit")
        sys.exit(1)
    
    input_csv = sys.argv[1]
    output_fit = sys.argv[2]
    
    convert_csv_to_fit(input_csv, output_fit)

if __name__ == "__main__":
    main()