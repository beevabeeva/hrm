# Heart Rate Monitor
### A simple heart rate monitor gui, so you can watch things in full screen while keeping an eye on your heart rate and heart rate zone.

### Dev Note 1
- currently re-writing using Ant+
- only problem is ant+ is 1 usb stick per application - so can't run GC at the same time as this. Guess I can add an entire workout mode...
- trying to see if I can connect to the trainer using bluetooth and run GC at the same time but GC will use ant+ only.
<insert example image>

I wish Golden Cheetah had an overlay mode. Having to toggle between full screen videos and GC is a pain, especially on macOS. It doesn't look like they'll be adding this feature anytime soon, so I'm doing it myself.

Using the `pycycling` library (https://github.com/zacharyedwardbull/pycycling), this program connects to a BLE heart rate monitor.
The colour of the BPM text changes based on HR zones.

The app can be used simultaneously while running Golden Cheetah.

### Note 1
Currently, it is hard coded to link to my hrm's unique address (you will have to find your devices ID and edit hrm.py to use it).
The HR zones are also currently hard coded. 
Adding a zone calculator based on age should be the next addition, as I will have to update it yearly.

### Note 2
Resizing, moving the window, and show/hide title bar are currently disabled, due to weird tkinter behaviour: if these things are enabled, ghosting of the text starts happening on click.
<insert example image>

### Note 3
The only way to watch things in full screen AND have something like this app overlayed on it is to use a browser like Firefox and enable it to manage full screen behaviour instead of letting MacOS move it to a new space (infuraiting).
In about:config, set `full-screen-api.macos-native-full-screen` to `False`.

### Note 4 
The code is currently horrendous and needs a lot of work.

### Note 5 
packaged for MacOS using py2app (ask GPT how)

### Note 6
Decided against having a close button to keep things minimal (I am watching video in the background afterall). You can quit using OS hotkeys or OS menu etc.

### ToDo
- [x] debug tkinter ghosting (try compile on Linux and see if issue persists)  (still not sure if overrideredirect bug only applies to Mac i.e test on Linux) (see https://stackoverflow.com/questions/63613253/how-to-disable-the-title-bar-in-tkinter-on-a-mac)
- [x] make window movable
- [x] make window resizable (adjusting font size) (done, but a bit sloppy)
- [ ] fix initial window size issue
- [ ] debug and fix pairing issue
- [ ] debug and fix crashing on first start issue (seems to happen once GC is already running but goes away after restarting hrm (app version))
- [ ] fix async code to make sense instead of being a hack 
- [x] add zone calculator based on age. (still need to check it works when packaged as app)
- [ ] maybe add power, speed and cadence.

---
### Full fledged training app requirements 
#### device
- [ ] BLE pair/connect/device saving
- [ ] ANT+ pair/connect/device saving (see https://github.com/Tigge/openant/blob/master/examples/multi_dev_fe_power_meter.py)
#### telemetry 
- [x] hrm
- [ ] power 
    - [ ] multiple power
- [ ] cadence
    - [ ] multiple cadence
- [ ] speed 
    - [ ] multiple speed
#### trainer
- [ ] slope (set)
    - [ ] slope profile editor
    - [ ] slope from gpx file
- [ ] ERG mode
#### data/stats
- [ ] data logger
- [ ] time
    - [ ] distance
    - [ ] averages
- [ ] save/export .FIT files
- [ ] Strava upload