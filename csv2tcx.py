# import csv
# import xml.etree.ElementTree as ET
# from datetime import datetime

# def convert_csv_to_tcx(csv_file, tcx_file):
#     # Create the root <TrainingCenterDatabase> element
#     tcx = ET.Element("TrainingCenterDatabase", xmlns="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", xsi="http://www.w3.org/2001/XMLSchema-instance")
    
#     # Create the <Activities> and <Activity> elements
#     activities = ET.SubElement(tcx, "Activities")
#     activity = ET.SubElement(activities, "Activity", Sport="Biking")
    
#     # Set activity ID (start time of activity)
#     activity_start_time = "2024-12-10T14:29:12Z"
#     activity_id = ET.SubElement(activity, "Id")
#     activity_id.text = activity_start_time

#     # Add creator information
#     creator = ET.SubElement(activity, "Creator", xsi_type="Device_t")
#     name = ET.SubElement(creator, "Name")
#     name.text = "Garmin TCX with Barometer"
#     unit_id = ET.SubElement(creator, "UnitId")
#     unit_id.text = "0"
#     product_id = ET.SubElement(creator, "ProductId")
#     product_id.text = "20119"
#     version = ET.SubElement(creator, "Version")
#     version_major = ET.SubElement(version, "VersionMajor")
#     version_major.text = "0"
#     version_minor = ET.SubElement(version, "VersionMinor")
#     version_minor.text = "0"
#     build_major = ET.SubElement(version, "BuildMajor")
#     build_major.text = "0"
#     build_minor = ET.SubElement(version, "BuildMinor")
#     build_minor.text = "0"

#     # Add Lap element with start time
#     lap_start_time = "2024-12-10T14:29:12Z"
#     lap = ET.SubElement(activity, "Lap", StartTime=lap_start_time)
    
#     # Add lap data
#     total_time_seconds = 4379
#     distance_meters = 30091.6
#     maximum_speed = 35
#     calories = 366
#     avg_heart_rate_bpm = 134
#     max_heart_rate_bpm = 154
#     intensity = "Active"
#     trigger_method = "Manual"
    
#     total_time = ET.SubElement(lap, "TotalTimeSeconds")
#     total_time.text = str(total_time_seconds)
#     distance = ET.SubElement(lap, "DistanceMeters")
#     distance.text = str(distance_meters)
#     max_speed = ET.SubElement(lap, "MaximumSpeed")
#     max_speed.text = str(maximum_speed)
#     lap_calories = ET.SubElement(lap, "Calories")
#     lap_calories.text = str(calories)
    
#     avg_hr = ET.SubElement(lap, "AverageHeartRateBpm")
#     avg_hr_value = ET.SubElement(avg_hr, "Value")
#     avg_hr_value.text = str(avg_heart_rate_bpm)
    
#     max_hr = ET.SubElement(lap, "MaximumHeartRateBpm")
#     max_hr_value = ET.SubElement(max_hr, "Value")
#     max_hr_value.text = str(max_heart_rate_bpm)
    
#     lap_intensity = ET.SubElement(lap, "Intensity")
#     lap_intensity.text = intensity
    
#     trigger = ET.SubElement(lap, "TriggerMethod")
#     trigger.text = trigger_method
    
#     # Create the <Track> element for the lap
#     track = ET.SubElement(lap, "Track")

#     # Open the CSV file and process the data
#     with open(csv_file, 'r') as file:
#         reader = csv.DictReader(file)
#         for row in reader:
#             timestamp = datetime.strptime(row['timestamp'], "%Y-%m-%d %H:%M:%S")
#             trackpoint = ET.SubElement(track, "Trackpoint")
#             time = ET.SubElement(trackpoint, "Time")
#             time.text = timestamp.isoformat() + "Z"  # Format timestamp as ISO 8601
            
#             # Add distance
#             distance_meters = row['distance']
#             distance = ET.SubElement(trackpoint, "DistanceMeters")
#             distance.text = str(distance_meters)
            
#             # Add heart rate
#             heart_rate_bpm = row['heart_rate']
#             heart_rate = ET.SubElement(trackpoint, "HeartRateBpm", xsi_type="HeartRateInBeatsPerMinute_t")
#             heart_rate_value = ET.SubElement(heart_rate, "Value")
#             heart_rate_value.text = heart_rate_bpm
            
#             # Add cadence
#             cadence = row['cadence']
#             cadence_element = ET.SubElement(trackpoint, "Cadence")
#             cadence_element.text = cadence
            
#             # Add Extensions (Speed and Watts)
#             extensions = ET.SubElement(trackpoint, "Extensions")
#             tpx = ET.SubElement(extensions, "TPX", xmlns="http://www.garmin.com/xmlschemas/ActivityExtension/v2")
#             speed = row['speed']
#             speed_element = ET.SubElement(tpx, "Speed")
#             speed_element.text = speed
#             watts = row['power']
#             watts_element = ET.SubElement(tpx, "Watts")
#             watts_element.text = watts

#     # Write the TCX file
#     tree = ET.ElementTree(tcx)
#     tree.write(tcx_file)

# # Example usage
# # convert_csv_to_tcx('workouts/csv/workout_20241210_162912.csv', 'output_activity.tcx')


import gzip
import shutil
import csv
from xml.etree.ElementTree import Element, SubElement, ElementTree
from datetime import datetime

def compress_to_gz(input_file, output_file):
    """
    Compress a .tcx file to .tcx.gz.
    
    Parameters:
        input_file (str): Path to the input .tcx file.
        output_file (str): Path for the compressed .tcx.gz file.
    """
    try:
        with open(input_file, 'rb') as f_in:
            with gzip.open(output_file, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        print(f"Successfully compressed {input_file} to {output_file}")
        return output_file
    except Exception as e:
        print(f"Error compressing file: {e}")
        return None


def convert_csv_to_tcx(input_csv, output_tcx):
    # Create the root XML element
    root = Element("TrainingCenterDatabase", {
        "xmlns": "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2",
        "xsi:schemaLocation": "http://www.garmin.com/xmlschemas/ActivityExtension/v2 http://www.garmin.com/xmlschemas/ActivityExtensionv2.xsd http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance"
    })

    # Create the Activities element
    activities = SubElement(root, "Activities")
    activity = SubElement(activities, "Activity", {"Sport": "Biking"})

    # Extract the activity date from the first row of the CSV file
    with open(input_csv, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        first_row = next(reader)
        activity_time = datetime.strptime(first_row["timestamp"], "%Y-%m-%d %H:%M:%S").isoformat() + "Z"
        SubElement(activity, "Id").text = activity_time

    # Add creator information
    creator = SubElement(activity, "Creator", {"xsi:type": "Device_t"})
    SubElement(creator, "Name").text = "Garmin TCX with Barometer"
    SubElement(creator, "UnitId").text = "0"
    SubElement(creator, "ProductId").text = "20119"
    version = SubElement(creator, "Version")
    SubElement(version, "VersionMajor").text = "0"
    SubElement(version, "VersionMinor").text = "0"
    SubElement(version, "BuildMajor").text = "0"
    SubElement(version, "BuildMinor").text = "0"

    # Create the Lap element
    lap = SubElement(activity, "Lap", {"StartTime": activity_time})
    total_distance = 0
    total_time_seconds = 0
    total_power = 0
    total_calories = 0  # Set appropriately if needed
    avg_heart_rate = []
    max_heart_rate = 0

    # Create the Track element
    track = SubElement(lap, "Track")
    with open(input_csv, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        previous_time = None
        for row in reader:
            timestamp = row["timestamp"]
            heart_rate = int(row["heart_rate"])
            power = int(row["power"])
            cadence = int(row["cadence"])
            speed = float(row["speed"])
            distance = float(row["distance"])

            # Update statistics
            total_distance = distance
            avg_heart_rate.append(heart_rate)
            max_heart_rate = max(max_heart_rate, heart_rate)
            total_power += power

            # Calculate time delta
            current_time = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
            if previous_time:
                total_time_seconds += (current_time - previous_time).total_seconds()
            previous_time = current_time

            # Create a Trackpoint
            trackpoint = SubElement(track, "Trackpoint")
            SubElement(trackpoint, "Time").text = current_time.isoformat() + "Z"
            SubElement(trackpoint, "DistanceMeters").text = f"{distance:.2f}"
            hr_element = SubElement(trackpoint, "HeartRateBpm", {"xsi:type": "HeartRateInBeatsPerMinute_t"})
            SubElement(hr_element, "Value").text = str(heart_rate)
            SubElement(trackpoint, "Cadence").text = str(cadence)

            # Add Extensions
            extensions = SubElement(trackpoint, "Extensions")
            tpx = SubElement(extensions, "TPX", {"xmlns": "http://www.garmin.com/xmlschemas/ActivityExtension/v2"})
            SubElement(tpx, "Speed").text = f"{speed:.2f}"
            SubElement(tpx, "Watts").text = str(power)

    # Finalize Lap statistics
    total_calories = total_power * total_time_seconds 
    SubElement(lap, "TotalTimeSeconds").text = f"{total_time_seconds:.2f}"
    SubElement(lap, "DistanceMeters").text = f"{total_distance:.2f}"
    SubElement(lap, "MaximumSpeed").text = f"{speed:.2f}"  # Assuming the last row contains max speed
    SubElement(lap, "Calories").text = str(total_calories)
    SubElement(lap, "AverageHeartRateBpm").text = str(sum(avg_heart_rate) // len(avg_heart_rate))
    SubElement(lap, "MaximumHeartRateBpm").text = str(max_heart_rate)
    SubElement(lap, "Intensity").text = "Active"
    SubElement(lap, "TriggerMethod").text = "Manual"

    # Write the XML to a file
    tree = ElementTree(root)
    with open(output_tcx, "wb") as fh:
        tree.write(fh, encoding="utf-8", xml_declaration=True)




# Example usage
# csv_to_tcx("input.csv", "output.tcx")
