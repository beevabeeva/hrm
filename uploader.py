#1 csv2fit - maybe this should be done straight after the csv is created?

#2 strava_token_refresh
from strava_token_refresh import strava_tokens

# strava_tokens()

#3 strava_upload.py #change to take in file 
from strava_upload import strava_upload
strava_upload()

#4 progress_bar.py
import tkinter as tk
from tkinter import ttk
import time
import random

class UploadApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Upload Progress")

        self.progress = ttk.Progressbar(root, orient=tk.HORIZONTAL, length=300, mode='determinate')
        self.progress.pack(pady=20)

        self.message = tk.StringVar()
        self.message_label = tk.Label(root, textvariable=self.message)
        self.message_label.pack(pady=10)

        self.start_upload_button = tk.Button(root, text="Start Upload", command=self.start_upload)
        self.start_upload_button.pack(pady=10)

    def start_upload(self):
        self.progress['value'] = 0 #reset bar
        self.start_upload_button.config(state=tk.DISABLED)
        self.message.set("Uploading...")
        self.root.after(100, self.simulate_upload)

    def simulate_upload(self):
        # Simulate an upload process
        self.progress['value'] += 20
        self.root.update_idletasks()

        if self.progress['value'] < 100:
            self.root.after(500, self.simulate_upload)  # Continue uploading
        else:
            self.complete_upload()

    def complete_upload(self):
        # Simulate an upload success or failure
        if random.choice([True, False]):
            self.upload_success()
        else:
            self.upload_failure("Network timeout")

    def upload_success(self):
        self.progress['value'] = 100
        self.message.set("Upload Complete")
        self.start_upload_button.config(state=tk.NORMAL)

    def upload_failure(self, error_message):
        self.message.set(f"Error: Upload Failed. {error_message}")
        self.start_upload_button.config(state=tk.NORMAL)
        self.progress['value'] = 0 #reset bar


if __name__ == "__main__":
    root = tk.Tk()
    app = UploadApp(root)
    root.mainloop()

#5 menu -> upload (latest)

#6 menu -> upload (select file)


