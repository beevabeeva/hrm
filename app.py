import asyncio
import tkinter as tk
from bleak import BleakClient
from pycycling.fitness_machine_service import FitnessMachineService
from pycycling.heart_rate_service import HeartRateService
import threading
import os
import json
import time
import csv
from datetime import datetime, timedelta

# convert csv to fit
# from csv2fit import convert_csv_to_fit
from csv2tcx import convert_csv_to_tcx, compress_to_gz


#Strava
from strava_token_refresh import strava_tokens
from strava_upload import strava_upload

import glob
def get_latest_file(directory, extension='*'):
    # Construct the search pattern with the given directory and file extension
    search_pattern = os.path.join(directory, f'*.{extension}')
    # Get the list of all files matching the pattern
    files = glob.glob(search_pattern)
    # Check if there are any files found
    if not files:
        message = f"Error: No files found in {directory}"
        messagebox.showerror('Python Error', message)
        return None
    # Find the latest file based on the last modified time
    latest_file = max(files, key=os.path.getmtime)
    print(latest_file)
    return latest_file


def open_settings():
    settings_window = tk.Toplevel(main_window)
    settings_window.title("Settings")

   


import tkinter.ttk as ttk
def update_status(message, progress_bar):
    # Update the label text safely from the main thread
    status_label.config(text=message)
    # Update the progress bar based on message content
    if "Upload ID" in message:
        progress_bar['value'] = 25
    elif "Upload status" in message:
        progress_bar['value'] += 25  # Example: you might adjust based on status
    elif "Activity is ready!" in message:
        progress_bar['value'] = 100
    elif "Error" in message:
        progress_bar['value'] = 0


from tkinter import filedialog
def upload_file():
    # Open a file dialog to choose a file
    file_path = filedialog.askopenfilename(
        initialdir="workouts/tcx",
        title="Select a TCX file",
        filetypes=[("TCX files", "*.tcx"), ("All files", "*.*")]
    )

    if file_path:
        # Call the upload function with the selected file
        # Use the file path directly
        upload_fn(path=file_path)

def upload_latest():
    upload_fn(latest=True)

def upload_fn(path=None,latest=False):
    upload_dialogue = tk.Toplevel(main_window)
    upload_dialogue.title("Upload to Strava")
    # Set the size of the dialog
    dialog_width = 400
    dialog_height = 200
    screen_width = upload_dialogue.winfo_screenwidth()
    screen_height = upload_dialogue.winfo_screenheight()
    x = (screen_width // 2) - (dialog_width // 2)
    y = (screen_height // 2) - (dialog_height // 2) - 300
    upload_dialogue.geometry(f"{dialog_width}x{dialog_height}+{x}+{y}")

    global status_label, progress_bar
    status_label = tk.Label(upload_dialogue, text="Starting upload...", wraplength=300, justify="center", padx=20, pady=20)
    status_label.pack()

    progress_bar = ttk.Progressbar(upload_dialogue, orient="horizontal", mode="determinate", length=300)
    progress_bar.pack(pady=10)
    progress_bar['value'] = 0  # Start at 0

    def upload():
        # nonlocal fit_path
        # nonlocal tcx_path
        # Refresh tokens
        strava_tokens()
        print(latest)
        tcx_path = None 
        if latest:
            # fit_path = get_latest_file("workouts/fit")
            tcx_path = get_latest_file("workouts/tcx")
            print(f"Latest file: {tcx_path}")  # DEBUG
        elif path:
            tcx_path = path
        # if fit_path:
        # strava_upload(fit_path, update_status, progress_bar)
        strava_upload(tcx_path, update_status, progress_bar)
        # Upload the latest file and update the label
        # strava_upload(fit_file, update_status,progress_bar)

    backend_thread = threading.Thread(target=upload)
    backend_thread.daemon = True  # Ensure the thread closes when the UI closes
    backend_thread.start()


#custom buttons
from math import pi, cos, sin
from typing import Iterator


class RoundedCanvas(tk.Canvas):
    minimum_steps = 10  # lower values give pixelated corners

    @staticmethod
    def get_cos_sin(radius: int) -> Iterator[tuple[float, float]]:
        steps = max(radius, RoundedCanvas.minimum_steps)
        for i in range(steps + 1):
            angle = pi * (i / steps) * 0.5
            yield (cos(angle) - 1) * radius, (sin(angle) - 1) * radius

    def create_rounded_box(self, x0: int, y0: int, x1: int, y1: int, radius: int, color: str) -> int:
        points = []
        cos_sin_r = tuple(self.get_cos_sin(radius))
        for cos_r, sin_r in cos_sin_r:
            points.append((x1 + sin_r, y0 - cos_r))
        for cos_r, sin_r in cos_sin_r:
            points.append((x1 + cos_r, y1 + sin_r))
        for cos_r, sin_r in cos_sin_r:
            points.append((x0 - sin_r, y1 + cos_r))
        for cos_r, sin_r in cos_sin_r:
            points.append((x0 - cos_r, y0 - sin_r))
        return self.create_polygon(points, fill=color, outline="")

class SimpleButton:
    def __init__(self, canvas, side, command=None, textvariable=None):
        self.canvas = canvas
        self.command = command
        self.side = side
        self.textvariable = textvariable
        self.default_color = "white"
        self.pressed_color = "gray"
        
        # Ensure canvas size is updated before creating button
        self.canvas.update_idletasks()
        self.create_button()

    def create_button(self):
        canvas_width = self.canvas.winfo_width()
        canvas_height = self.canvas.winfo_height()

        # Ensure canvas dimensions are valid
        if canvas_width <= 0 or canvas_height <= 0:
            raise ValueError("Canvas width and height must be positive")

        # Determine the button position and size
        if self.side == "left":
            x1, x2 = 0, canvas_width // 4
        elif self.side == "right":
            x1, x2 = canvas_width - canvas_width // 4, canvas_width
        else:
            raise ValueError("Side must be 'left' or 'right'")
        
        y1, y2 = 0, canvas_height

        # Create the button rectangle and text
        self.rect_id = self.canvas.create_rounded_box(x1, y1, x2, y2, radius=10, color="white")
        self.text_id = self.canvas.create_text((x1 + x2) // 2, (y1 + y2) // 2, text=self.textvariable.get(), fill="black", font=("Arial", 12))

        # Bind click events
        self.canvas.tag_bind(self.rect_id, "<Button-1>", self.on_click)
        self.canvas.tag_bind(self.text_id, "<Button-1>", self.on_click)
        self.canvas.tag_bind(self.rect_id, "<ButtonPress-1>", self.on_press)
        self.canvas.tag_bind(self.text_id, "<ButtonPress-1>", self.on_press)
        self.canvas.tag_bind(self.rect_id, "<ButtonRelease-1>", self.on_release)
        self.canvas.tag_bind(self.text_id, "<ButtonRelease-1>", self.on_release)


    def resize_button(self, event=None):
        # Remove the existing rectangle and text
        self.canvas.delete(self.rect_id)
        self.canvas.delete(self.text_id)

        # Recreate the button with updated dimensions
        self.create_button()

    def on_press(self, event):
        self.canvas.itemconfig(self.rect_id, fill=self.pressed_color)

    def on_release(self, event):
        self.canvas.itemconfig(self.rect_id, fill=self.default_color)
        self.on_click(event)  # Trigger the click event after releasing the button

    def on_click(self, event):
        if self.command:
            self.command()

LAYOUT_FILE = "window_layout.json"

def save_layout(windows):
    layout = {}
    for title, window in windows.items():
        geometry = window.geometry()
        font_size = window.winfo_children()[1].cget("font").split()[1]  # Get font size from label
        layout[title] = {"geometry": geometry, "font_size": font_size}
        print(f"Saving {title}: geometry={geometry}, font_size={font_size}")

    with open(LAYOUT_FILE, "w") as f:
        json.dump(layout, f)
    print("Layout saved successfully!")


def load_layout():
    if os.path.exists(LAYOUT_FILE):
        with open(LAYOUT_FILE, "r") as f:
            return json.load(f)
    return None


class BluetoothDataHandler:
    def __init__(self):
        self.heart_rate_value = None
        self.power_value = None
        self.cadence_value = None
        self.speed_value = None
        self.distance_value = None
        self.timer_value = None
        self.timer_running = False
        self.start_time = 0
        self.elapsed_time = 0
        self.total_distance = 0
        self.csv_file = None
        self.csv_writer = None
        self.start_datetime = None
        self.distance_value = None
        self.total_distance = 0
        self.last_update_time = None
        #bike-pace variables
        self.min_per_km_value = None
        # self.min_per_m_value = None
        self.km_per_min_value = None  # New variable for km/min
        self.last_km_distance = 0
        self.last_m_distance = 0
        self.last_km_time = 0
        self.last_m_time = 0
        #kcals
        self.calories_burned = 0
        self.calories_value = None
        self.last_kj_time = None

        

    def update_indoor_bike_data(self, data):
        current_time = time.time()
        # Update distance
        if self.timer_running:  # Check if the timer is running
            if self.last_update_time is not None:
                time_diff = current_time - self.last_update_time
                self.total_distance += (data.instant_speed / 3.6) * time_diff  # Convert km/h to m/s

            self.last_update_time = current_time

            if self.distance_value is not None:
                self.distance_value.set(f"{self.total_distance / 1000:.2f}")  # Display in km

            



            # update bike-pace (min/km and min/m) metrics
            if self.total_distance - self.last_km_distance >= 1000:  # Every kilometer
                time_diff = current_time - self.last_km_time
                min_per_km = (time_diff / 60) / ((self.total_distance - self.last_km_distance) / 1000)
                if self.min_per_km_value is not None:
                    self.min_per_km_value.set(f"{min_per_km:.2f}")
                self.last_km_distance = self.total_distance
                self.last_km_time = current_time

            # if self.total_distance - self.last_m_distance >= 1:  # Every meter
            #     time_diff = current_time - self.last_m_time
            #     min_per_m = (time_diff / 60)
            #     if self.min_per_m_value is not None:
            #         self.min_per_m_value.set(f"{min_per_m:.2f}")
            #     self.last_m_distance = self.total_distance
            #     self.last_m_time = current_time

            # Update km/min estimate based on speed
            if data.instant_speed > 0:
                km_per_min = data.instant_speed / 60  # Convert km/h to km/min
                if self.km_per_min_value is not None:
                    self.km_per_min_value.set(f"{km_per_min:.2f}")



        if self.power_value is not None:
            power = data.instant_power
            self.power_value.set(power)
            #updae calories
            if self.timer_running:
                # self.calories_burned += (power/3600) / 4.184
                if self.last_update_time is not None:
                    time_diff = current_time - self.last_kj_time
                    energy_j = power * time_diff
                    self.calories_burned += energy_j / 1000  # Convert J to kJ
                if self.calories_value is not None:
                    self.calories_value.set(f"{self.calories_burned:.1f}")
            self.last_kj_time = current_time

        if self.cadence_value is not None:
            self.cadence_value.set(data.instant_cadence)
        if self.speed_value is not None:
            self.speed_value.set(data.instant_speed)
        if self.distance_value is not None:
            self.distance_value.set(f"{self.total_distance / 1000:.2f}")  # Display in km
          
        # Log data to CSV file
        if self.csv_writer and self.timer_running:
            self.csv_writer.writerow({
                'timestamp': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                'heart_rate': self.heart_rate_value.get() if self.heart_rate_value else '',
                'power': self.power_value.get() if self.power_value else '',
                'cadence': self.cadence_value.get() if self.cadence_value else '',
                'speed': self.speed_value.get() if self.speed_value else '',
                'distance': self.total_distance
            })

    def update_heart_rate(self, data):
        if self.heart_rate_value is not None:
            self.heart_rate_value.set(data.bpm)

    def reset_calories(self):
        self.calories_burned = 0
        if self.calories_value is not None:
            self.calories_value.set("0")

    def start_timer(self):
        current_time = time.time()
        if not self.timer_running:
            self.timer_running = True
            self.start_time = current_time - self.elapsed_time
            self.last_update_time = current_time  # Set last update time to current time to avoid jumps
            self.update_timer()
            if not self.csv_file:
                self.start_csv_logging()

    def pause_timer(self):
        if self.timer_running:
            self.timer_running = False
            self.elapsed_time = time.time() - self.start_time
            self.last_update_time = None  # Reset last update time when paused

    def stop_timer(self):
        self.timer_running = False
        self.elapsed_time = 0
        if self.timer_value is not None:
            self.timer_value.set("00:00:00")
        self.reset_distance()
        self.reset_calories()
        self.finish_csv_logging()


    def update_timer(self):
        if self.timer_running:
            elapsed = int(time.time() - self.start_time)
            hours, rem = divmod(elapsed, 3600)
            minutes, seconds = divmod(rem, 60)
            time_str = f"{hours:02d}:{minutes:02d}:{seconds:02d}"
            if self.timer_value is not None:
                self.timer_value.set(time_str)
            threading.Timer(1, self.update_timer).start()

    def reset_distance(self):
        self.total_distance = 0
        if self.distance_value is not None:
            self.distance_value.set("0.00")
        self.last_update_time = None
        # reset bike-pace metrics
        self.last_km_distance = 0
        self.last_m_distance = 0
        self.last_km_time = time.time()
        self.last_m_time = time.time()
        if self.min_per_km_value is not None:
            self.min_per_km_value.set("0.00")
        # if self.min_per_m_value is not None:
        #     self.min_per_m_value.set("0.00")
        if self.km_per_min_value is not None:
            self.km_per_min_value.set("0")

    def start_csv_logging(self):
        self.start_datetime = datetime.now()
        directory = "workouts/csv"
        if not os.path.exists(directory):
            os.makedirs(directory)
        filename = os.path.join(directory, f"workout_{self.start_datetime.strftime('%Y%m%d_%H%M%S')}.csv")
        self.csv_file = open(filename, 'w', newline='')
        self.csv_writer = csv.DictWriter(self.csv_file, fieldnames=['timestamp', 'heart_rate', 'power', 'cadence', 'speed', 'distance'])
        self.csv_writer.writeheader()

    def finish_csv_logging(self):
        if self.csv_file:
            csv_file_path = self.csv_file.name
            self.csv_file.close()
            print(f"CSV file saved: {csv_file_path}")
            self.csv_file = None
            self.csv_writer = None
            #convert csv to fit
            # fit_file_path = os.path.splitext(csv_file_path)[0] + '.fit'
            base_name = os.path.splitext(os.path.basename(csv_file_path))[0]
            # fit_file_path = os.path.join('workouts/fit', base_name + '.fit')
            tcx_file_path = os.path.join('workouts/tcx', base_name + '.tcx')
            # convert_csv_to_fit(csv_file_path, fit_file_path)
            convert_csv_to_tcx(csv_file_path, tcx_file_path)
            # print(f"FIT file saved: {fit_file_path}")
            print(f"TCX file saved: {tcx_file_path}")


def create_custom_title_bar(window, title):
    title_label = tk.Label(window, text=title, bg="#2B2B2B", fg="white") # maybe set  bg="systemTransparent" in future...
    title_label.pack(fill='both')

    def start_move(event):
        window.x = event.x
        window.y = event.y

    def stop_move(event):
        window.x = None
        window.y = None

    def do_move(event):
        x = window.winfo_pointerx() - window.x
        y = window.winfo_pointery() - window.y
        window.geometry(f"+{x}+{y}")

    title_label.bind("<ButtonPress-1>", start_move)
    title_label.bind("<ButtonRelease-1>", stop_move)
    title_label.bind("<B1-Motion>", do_move)

def create_value_window(title, x_offset, y_offset, handler_value, layout=None):

    def current_font_size():
        return (int(label.cget("font").split()[1]))

    def adjust_text_size(event=None):
        
        # if not hasattr(window, 'user_font_size'):
        new_width = window.winfo_width()
        new_height = window.winfo_height()
        font_percentage = 0.7
        new_font_size = max(int(min(new_width, new_height) * font_percentage), current_font_size())
        label.config(font=("Helvetica", new_font_size, "bold"), padx=0, pady=0, anchor="n")
        window.user_font_size = new_font_size
        slider.set(new_font_size)  # Synchronize slider with the calculated font
        # else:
            # label.config(font=("Helvetica", window.user_font_size, "bold"), padx=0, pady=0, anchor="n")

    def adjust_text_size2(event=None):
        font_info = label.cget("font")
        font_size = font_info.split()[1]

        width = window.winfo_width()
        height = window.winfo_height()

        a = int(height / 2)
        buff = len(str(label.cget("text")))
        b = int(width / (buff if buff >= 12 else 12))
        new_font_size = min(a, b)
        if new_font_size != font_size:
            font_size = int(new_font_size+0.7*new_font_size)
            label.config(font=("Helvetica", font_size, "bold"), padx=0, pady=0,anchor="n", justify="center")


    import customtkinter as ctk
    def show_slider(event):
        global slider_window, slider
        if slider_window and slider_window.winfo_ismapped():
            slider_window.withdraw()
            return

        # Create a new transparent window
        slider_window = tk.Toplevel()
        slider_window.wm_overrideredirect(True)
        slider_window.attributes('-topmost', True)
        slider_window.attributes('-transparent', True)

        # Position the slider window next to the clicked window
        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight() 

        # Get the window dimensions
        window_width = window.winfo_width()
        window_height = window.winfo_height()

        x = event.x_root 
        y = event.y_root

        slider_width = 200
        slider_height = 80

        # Get the window dimensions and position
        window_width = window.winfo_width()
        window_height = window.winfo_height()
        window_x = window.winfo_rootx()
        window_y = window.winfo_rooty()

        # Calculate the position for the slider window
        x = window_x + window_width + 10  # Position it to the right of the window
        y = window_y  # Align with the top of the window

        # Check if the slider would go off the right edge of the screen
        if x + 200 > screen_width:
            x = window_x - 200 - 10  # Move it to the left of the window if necessary

        # Check if the slider would go off the bottom edge of the screen
        if y + 80 > screen_height:
            y = screen_height - 80  # Adjust y to stay within screen height

        slider_window.geometry(f'{slider_width}x{slider_height}+{x}+{y}')  # Increased height to accommodate the button

        # Create a CTkSlider
        slider = ctk.CTkSlider(slider_window, from_=10, to=200, command=adjust_font_size)
        # font_info = label.cget("font")
        font_size = current_font_size() #int(font_info.split()[1])
        slider.set(font_size)  # Set slider's initial value to the current font size
        slider.pack(pady=5, padx=10)

        # Create an OK button
        ok_button = tk.Button(slider_window, text="OK", command=save_font_size)
        ok_button.pack(pady=0, padx=0)

        # Adjust the slider range based on the window size
        # update_slider_range()

    def adjust_font_size(value):
        new_size = int(float(value))
        label.config(font=("Helvetica", new_size, "bold"))
        # font = tk.font.Font(family="Helvetica", size=new_size, weight="bold")
        # print(f"ascent: {font.metrics("ascent")}")
        # print(f"descent: {font.metrics("descent")}")

    def update_slider_range():
        global slider
        new_win_max = min(window.winfo_width(), window.winfo_height()) 
        new_max = max(new_win_max,current_font_size()) # if font is bigger than window size...
        new_min = min(10, current_font_size())
        slider.configure(from_= new_min, to=new_max)

    def save_font_size():
        # global current_font_size
        # # Update the global font size variable with the slider value
        # current_font_size = int(float(slider.get()))
        # label.config(font=("Helvetica", current_font_size, "bold"))
        window.user_font_size = int(float(slider.get()))
        label.config(font=("Helvetica", window.user_font_size, "bold"))
        destroy_slider()

    def destroy_slider(event=None):
        global slider_window
        if slider_window:
            slider_window.destroy()
            slider_window = None

    def hide_slider(event):
        if slider_window and slider_window.winfo_ismapped():
            # Check if click is inside the main window
            if not (window.winfo_rootx() <= event.x_root <= window.winfo_rootx() + window.winfo_width() and
                    window.winfo_rooty() <= event.y_root <= window.winfo_rooty() + window.winfo_height()):
                destroy_slider()

                
    window = tk.Toplevel()
    window.overrideredirect(True)
    create_custom_title_bar(window, title)
    
    if layout and title in layout:
        window.geometry(layout[title]["geometry"])
    else:
        window.geometry(f"300x200+{x_offset}+{y_offset}")

    # Load the saved font size or use a default
    font_size = 20
    if layout and title in layout:
        font_size = int(layout[title]["font_size"])
    window.user_font_size = font_size  # Store the initial font size

    window.update_idletasks()
    window.wm_attributes("-topmost", True)
    window.resizable(True, True)
    window.minsize(width=50, height=50)
    window.wm_attributes('-transparent', True)

    value = tk.IntVar(window, value=0)
    setattr(handler, handler_value, value)

    label = tk.Label(window, textvariable=value, bg="systemTransparent", font=("Arial", font_size,"bold"), bd=0,justify="center",anchor="n")
    label.pack(expand=True, fill="both", side="top",pady=0)
    global slider_window, slider, current_font_size_var
    slider_window = None
    slider = None
    current_font_size_var = current_font_size()
    window.bind('<Button-2>', show_slider)

    # window.bind("<Configure>", adjust_text_size) # this causes font set by slider to not be saved
    # adjust_text_size()
    
    return window





def create_timer_window(handler, x_offset, y_offset, layout=None):
    #functions
    def adjust_text_size(event=None):
        new_width = window.winfo_width()
        new_height = window.winfo_height()
        font_percentage = 0.5
        new_font_size = int(min(new_width, new_height) * font_percentage)
        timer_label.config(font=("Helvetica", new_font_size, "bold"), padx=0, pady=0)

    def stop_timer_and_reset_button():
        handler.stop_timer()
        start_btn_text.set("Start")

    def toggle_start_pause(event=None):
        if handler.timer_running:
            handler.pause_timer()
            start_btn_text.set("Resume")
        else:
            handler.start_timer()
            start_btn_text.set("Pause")

    def resize_buttons(event):
        start_pause_button.resize_button()
        stop_button.resize_button()

    # window code
    window = tk.Toplevel()
    window.overrideredirect(True)
    create_custom_title_bar(window, "Workout Timer")

    if layout and "Timer" in layout:
        window.geometry(layout["Timer"]["geometry"])
    else:
        window.geometry(f"300x250+{x_offset}+{y_offset}")
    
    window.update_idletasks()
    window.wm_attributes("-topmost", True)
    window.resizable(True, True)
    window.minsize(width=50, height=50)
    window.wm_attributes('-transparent', True)

    timer_value = tk.StringVar(window, value="00:00:00")
    handler.timer_value = timer_value

    timer_label = tk.Label(window, textvariable=timer_value, bg="systemTransparent", font=("Arial", 12), bd=0)
    timer_label.pack(expand=True, fill="both")

    button_frame = RoundedCanvas(window, width=400, height=100, bg="systemTransparent",bd=0,highlightthickness=0)
    button_frame.pack(fill="both", expand=True)

    # # Create custom Start/Pause button
    start_btn_text = tk.StringVar(window, value="Start")
    start_pause_button = SimpleButton(button_frame, side="left", command=toggle_start_pause, textvariable=start_btn_text)
 
    # # Create custom Stop button
    stop_button = SimpleButton(button_frame, side="right", textvariable=tk.StringVar(value="Stop"),
                               command=stop_timer_and_reset_button)

    def update_start_pause_button_text(*args):
        start_pause_button.canvas.itemconfig(start_pause_button.text_id, text=start_btn_text.get())

    start_btn_text.trace("w", update_start_pause_button_text)
    
    window.bind("<Configure>", adjust_text_size)
    button_frame.bind("<Configure>", resize_buttons)
    adjust_text_size()

    return window

async def run_bluetooth(ftms_address, hr_address, handler):
    async with BleakClient(ftms_address, timeout=5) as ftms_client, BleakClient(hr_address, timeout=10) as hr_client:
        ftms = FitnessMachineService(ftms_client)
        hr_service = HeartRateService(hr_client)

        def print_indoor_bike_data(data):
            handler.update_indoor_bike_data(data)

        def print_heart_rate_data(data):
            handler.update_heart_rate(data)

        ftms.set_indoor_bike_data_handler(print_indoor_bike_data)
        await ftms.enable_indoor_bike_data_notify()

        await ftms.enable_control_point_indicate()
        await ftms.request_control()
        await ftms.reset()

        hr_service.set_hr_measurement_handler(print_heart_rate_data)
        await hr_service.enable_hr_measurement_notifications()

        while True:
            await asyncio.sleep(1)

def start_bluetooth(ftms_address, hr_address, handler):
    def bluetooth_thread_function():
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        print(f"Attempting connection to devices... ")
        while True:
            try:
                loop.run_until_complete(run_bluetooth(ftms_address, hr_address, handler))
            except Exception as e:
                print(f"Bluetooth Error, retrying... {e}")
                continue

    bluetooth_thread = threading.Thread(target=bluetooth_thread_function, daemon=True)
    bluetooth_thread.start()

# Main window setup
main_window = tk.Tk()
main_window.withdraw()

# Create menu bar
menu_bar = tk.Menu(main_window)
main_window.config(menu=menu_bar)

# Create File menu
file_menu = tk.Menu(menu_bar, tearoff=0)
menu_bar.add_cascade(label="File", menu=file_menu)

# Add Settings menu to the menu bar
settings_menu = tk.Menu(menu_bar, tearoff=0)
menu_bar.add_cascade(label="Settings", menu=settings_menu)
settings_menu.add_command(label="Open Settings", command=open_settings) #TODO: add hr zones/age settins

# Create Strava menu
strava_menu = tk.Menu(menu_bar, tearoff=0)
menu_bar.add_cascade(label="Strava", menu=strava_menu)
strava_menu.add_command(label="Upload latest", command=upload_latest)
strava_menu.add_command(label="Choose file", command=upload_file)


# Create an instance of the data handler
handler = BluetoothDataHandler()

# Load saved layout
saved_layout = load_layout()

# Create the windows
windows = {
    "Heart Rate": create_value_window("Heart Rate", 100, 0, "heart_rate_value", saved_layout),
    "Power": create_value_window("Power", 600, 0, "power_value", saved_layout),
    "Cadence": create_value_window("Cadence", 100, 400, "cadence_value", saved_layout),
    "Speed": create_value_window("Speed", 600, 400, "speed_value", saved_layout),
    "Distance": create_value_window("Distance", 350, 600, "distance_value", saved_layout),
    "Timer": create_timer_window(handler, 350, 200, saved_layout),
    "min/km": create_value_window("min/km", 100, 800, "min_per_km_value", saved_layout),
    "km/m": create_value_window("km/m", 600, 800, "km_per_min_value", saved_layout),
    "kcal": create_value_window("kcal", 350, 800, "calories_value", saved_layout)
}



# Add Save Layout option to File menu
file_menu.add_command(label="Save Layout", command=lambda: save_layout(windows))



# Start the Bluetooth process
suito_adr = "0E0DB68C-0E0A-2490-11A1-2A4F4925151C"
heart_rate_adr = "F1982529-2BF7-810D-ED72-372360D681F3"  
start_bluetooth(suito_adr, heart_rate_adr, handler)

# Save layout on exit
def on_closing():
    save_layout(windows) # NOTE: this does not work!!
    print("saved layout")
    main_window.destroy()

main_window.protocol("WM_DELETE_WINDOW", on_closing)

# Start the Tkinter event loop
main_window.mainloop()
