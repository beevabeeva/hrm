import tkinter as tk
import customtkinter as ctk

def show_slider(event):
    global slider_window, slider
    if slider_window and slider_window.winfo_ismapped():
        slider_window.withdraw()
        return

    # Create a new transparent window
    slider_window = tk.Toplevel()
    slider_window.wm_overrideredirect(True)
    slider_window.attributes('-topmost', True)
    slider_window.attributes('-transparent', True)

    # Position the slider window next to the clicked window
    x = window.winfo_rootx() + event.x_root + 20
    y = window.winfo_rooty() + event.y_root

    slider_window.geometry(f'200x80+{x}+{y}')  # Increased height to accommodate the button

    # Create a CTkSlider
    slider = ctk.CTkSlider(slider_window, from_=10, to=100, command=adjust_font_size)
    slider.set(current_font_size)  # Set slider's initial value to the current font size
    slider.pack(pady=5, padx=10)

    # Create an OK button
    ok_button = tk.Button(slider_window, text="OK", command=save_font_size)
    ok_button.pack(pady=0, padx=0)

    # Adjust the slider range based on the window size
    # update_slider_range()

def adjust_font_size(value):
    new_size = int(float(value))
    label.config(font=("Helvetica", new_size, "bold"))

def update_slider_range():
    global slider
    new_max = min(window.winfo_width(), window.winfo_height()) 
    slider.configure(from_=10, to=new_max)

def save_font_size():
    global current_font_size
    # Update the global font size variable with the slider value
    current_font_size = int(float(slider.get()))
    label.config(font=("Helvetica", current_font_size, "bold"))
    destroy_slider()

def destroy_slider(event=None):
    global slider_window
    if slider_window:
        slider_window.destroy()
        slider_window = None

def hide_slider(event):
    if slider_window and slider_window.winfo_ismapped():
        # Check if click is inside the main window
        if not (window.winfo_rootx() <= event.x_root <= window.winfo_rootx() + window.winfo_width() and
                window.winfo_rooty() <= event.y_root <= window.winfo_rooty() + window.winfo_height()):
            destroy_slider()

# Initialize the main window
# window = tk.Tk()
# window.geometry('300x200')
# window.title('Main Window')

# # Initialize global font size variable
# current_font_size = 20  # Default font size

# label = tk.Label(window, text="Sample Text", font=("Helvetica", current_font_size, "bold"))
# label.pack(expand=True, fill=tk.BOTH)

# # Initialize slider window
# slider_window = None

# # Bind click events to show and hide slider
# window.bind('<Button-2>', show_slider)  # Show slider on left click
# # window.bind('<Button-1>', hide_slider)  # Hide slider on right click gues we don't need this if we have a button

# window.mainloop()
