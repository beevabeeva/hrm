# much better, simpler example from '10 Rep' https://stackoverflow.com/questions/61687529/how-do-i-open-multiple-tk-windows-at-the-same-time

from tkinter import *
window = Tk()
window.title("window1")
# def open_new_window():
#     window2 = Toplevel()
#     window2.title("Window2")

#     window.after(1000, open_new_window)
# open_new_window()
window2 = Toplevel()
window2.title("Window2")

# initial position for windows (later on will add function to load positions from a saved layout file if it exists)
window.geometry("+50+50")
window2.geometry("+300+50")
window.wm_attributes("-topmost", True) # tried hacking mac with extensions, does not work. have to do this to keep window on top so I can watch things. must come after transparency
window2.wm_attributes("-topmost", True) # tried hacking mac with extensions, does not work. have to do this to keep window on top so I can watch things. must come after transparency

window.mainloop()
