
# much better, simpler example based on '10 Rep' https://stackoverflow.com/questions/61687529/how-do-i-open-multiple-tk-windows-at-the-same-time
# trying to implement this 

from tkinter import *



class HeartRateMonitorApp:
    def __init__(self, master):
        master.wm_attributes("-topmost", True) # have to do this here before anything else, otherwise it does not work (i.e window does not stay on top of all others)

        self.master = master
        #window stuff:
        master.overrideredirect(True)  # Remove system toolbar
        master.title("Heart Rate Monitor")
        master.resizable(True, True)
        # master.bind("<Configure>", self.adjust_text_size)
        master.wm_attributes('-transparent',True)
        master.minsize(width= master.winfo_screenwidth() //10, height=master.winfo_screenheight()// 10)
        master.wm_attributes("-topmost", True) # have to do this here before anything else, otherwise it does not work (i.e window does not stay on top of all others)

class HeartRateMonitorApp2:
    def __init__(self, master):
        master.wm_attributes("-topmost", True) # have to do this here before anything else, otherwise it does not work (i.e window does not stay on top of all others)

        self.master = master
        #window stuff:
        master.overrideredirect(True)  # Remove system toolbar
        master.title("Heart Rate Monitor")
        master.resizable(True, True)
        # master.bind("<Configure>", self.adjust_text_size)
        master.wm_attributes('-transparent',True)
        master.minsize(width= master.winfo_screenwidth() //10, height=master.winfo_screenheight()// 10)
        master.wm_attributes("-topmost", True) # have to do this here before anything else, otherwise it does not work (i.e window does not stay on top of all others)

root = Tk()
app = HeartRateMonitorApp(root)

app2 = HeartRateMonitorApp(root)
# window2 = Toplevel()
# window2.title("Window2")

# # initial position for windows (later on will add function to load positions from a saved layout file if it exists)
root.app.geometry("+50+50")
root.app2.geometry("+300+50")

root.mainloop()
