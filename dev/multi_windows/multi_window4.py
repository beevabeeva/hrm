# all examples are slightly off. trying gpt to debug them.

import tkinter as tk
from openant.easy.node import Node
from openant.devices import ANTPLUS_NETWORK_KEY
from openant.devices.heart_rate import HeartRate, HeartRateData
from openant.devices.power_meter import PowerMeter, PowerData
import threading

#-------- `Global Functions`------------

def start_ant(node, device_class, on_device_data):
    def ant_thread_function():
        device = device_class(node)
        device.on_found = lambda: print(f"Device {device} found and receiving")
        device.on_device_data = on_device_data

        try:
            # device.open() #gpt making shit up
            node.start()
        except KeyboardInterrupt:
            print("Closing ANT+ device...")
        finally:
            device.close_channel()

    ant_thread = threading.Thread(target=ant_thread_function, daemon=True)
    ant_thread.start()

# Callback function to handle  ANT+ data data
class AntDataHandler:
    def __init__(self):
        self.heart_rate_value = None
        self.power_value = None
        self.cadence_value = None

    def on_heart_rate_data(self, page: int, page_name: str, data):
        if isinstance(data, HeartRateData):
            if self.heart_rate_value is not None:
                self.heart_rate_value.set(data.heart_rate)

    def on_power_data(self, page: int, page_name: str, data):
        if isinstance(data, PowerData):
            if self.power_value is not None:
                self.power_value.set(data.instantaneous_power)
            if self.cadence_value is not None:
                self.cadence_value.set(data.cadence)


# Function to create a custom title bar
def create_custom_title_bar(window, title):
    
    # Add a label to the title bar with the window title (using frames and canvas sucks - can't centre the text)
    title_label = tk.Label(window, text=title, bg="#2B2B2B", fg="white")
    title_label.pack(fill='both') # have to fill so the whole area is clickable - like a title bar

    # Make the window draggable by the title bar
    def start_move(event):
        window.x = event.x
        window.y = event.y

    def stop_move(event):
        window.x = None
        window.y = None

    def do_move(event):
        x = window.winfo_pointerx() - window.x
        y = window.winfo_pointery() - window.y
        window.geometry(f"+{x}+{y}")

    title_label.bind("<ButtonPress-1>", start_move)
    title_label.bind("<ButtonRelease-1>", stop_move)
    title_label.bind("<B1-Motion>", do_move)


#---------Specific Window Functions--------------

# Function to create the first window
def create_heart_rate_window(handler):

    def update_heart_rate_value():
        if heart_rate_value.get() is not None:
            current_value = heart_rate_value.get()
            heart_rate_value.set(current_value)
        first_window.after(1000, update_heart_rate_value)

    # def on_heart_rate_data(page: int, page_name: str, data):
    #     if isinstance(data, HeartRateData):
    #         heart_rate_value.set(data.heart_rate)


    def adjust_text_size(event=None):
        current_font_size=int(heart_rate_label.cget("font").split()[1])
        new_width = first_window.winfo_width()
        new_height =  first_window.winfo_height()
        font_percentage = 0.7  
        new_font_size = int(min(new_width, new_height) * font_percentage) # Calculate the new font size based on the window size
        heart_rate_label.config(font=("Helvetica", new_font_size,"bold"),padx=0, pady=0)  # Set the new font size for the label
  

    first_window = tk.Toplevel()
    first_window.overrideredirect(True)  # Remove the default title bar
    create_custom_title_bar(first_window, "Hear Rate")    
     # Position the window
    first_window.geometry("300x200+100+0")  # Width x Height + X Offset + Y Offset
    # Ensure the window is always on top
    first_window.update_idletasks()  # Ensure the window is fully initialized
    first_window.wm_attributes("-topmost", True)
    first_window.resizable(True, True)
    first_window.wm_attributes('-transparent',True) #both label and window must be configured for transparency to work
    # Add content to the window 
    heart_rate_value = tk.IntVar(first_window, value=0)
    handler.heart_rate_value = heart_rate_value
    # Add content to the window
    heart_rate_label = tk.Label(first_window, textvariable=heart_rate_value, bg="systemTransparent", font=("Arial", 20),bd=0)
    heart_rate_label.pack(expand=True, fill="both")
    # Start updating the hr value
    # update_heart_rate_value()  

    # update text size
    first_window.bind("<Configure>", adjust_text_size)
    adjust_text_size()
    # start new ant connection in new thread
    # start_ant(node,HeartRate, on_heart_rate_data)
    



# Function to create the second window
def create_power_window(handler):

    def update_power_value():
        if power_value.get() is not None:
            current_value = power_value.get()
            power_value.set(current_value)
        second_window.after(1000, update_power_value)

    def adjust_text_size(event=None):
        current_font_size=int(power_label.cget("font").split()[1])
        new_width = second_window.winfo_width()
        new_height =  second_window.winfo_height()
        font_percentage = 0.7  
        new_font_size = int(min(new_width, new_height) * font_percentage) # Calculate the new font size based on the window size
        power_label.config(font=("Helvetica", new_font_size,"bold"),padx=0, pady=0)  # Set the new font size for the label
    
    second_window = tk.Toplevel()
    second_window.overrideredirect(True)  # Remove the default title bar
    create_custom_title_bar(second_window, "Power")
    second_window.geometry("300x200+600+0")  # Width x Height + X Offset + Y Offset
    # Ensure the window is always on top
    second_window.update_idletasks()  # Ensure the window is fully initialized
    second_window.wm_attributes("-topmost", True)
    second_window.resizable(True, True)
    second_window.wm_attributes('-transparent',True) #both label and window must be configured for transparency to work
    # Add content to the window 
    power_value = tk.IntVar(second_window,value=0)  # Initialize with value 0
    handler.power_value = power_value
    power_label = tk.Label(second_window, textvariable=power_value, bg="systemTransparent", font=("Arial", 20),bd=0)
    power_label.pack(expand=True, fill="both")
    update_power_value()  # Start updating the power value
    #adjust text size
    second_window.bind("<Configure>", adjust_text_size)
    adjust_text_size()
     # start new ant connection in new thread
    # cadence_value = tk.IntVar(second_window,value=0) #hack 
    # start_ant(node,PowerMeter, on_power_data)

# Create Cadence window
def create_cadence_window(handler):

    def update_cadence_value():
        if cadence_value.get() is not None:
            current_value = cadence_value.get()
            cadence_value.set(current_value)
        cadence_window.after(1000, update_cadence_value)

    # def on_cadence_data(page: int, page_name: str, data):
    #     if isinstance(data, PowerData):
    #         cadence_value.set(data.cadence) 
    #         power_value.set(data.instantaneous_power)

    def adjust_text_size(event=None):
        current_font_size=int(cadence_label.cget("font").split()[1])
        new_width = cadence_window.winfo_width()
        new_height =  cadence_window.winfo_height()
        font_percentage = 0.7  
        new_font_size = int(min(new_width, new_height) * font_percentage) # Calculate the new font size based on the window size
        cadence_label.config(font=("Helvetica", new_font_size,"bold"),padx=0, pady=0)  # Set the new font size for the label
    
    cadence_window = tk.Toplevel()
    cadence_window.overrideredirect(True)  # Remove the default title bar
    create_custom_title_bar(cadence_window, "Cadence")
    cadence_window.geometry("300x200+600+400")  # Width x Height + X Offset + Y Offset
    # Ensure the window is always on top
    cadence_window.update_idletasks()  # Ensure the window is fully initialized
    cadence_window.wm_attributes("-topmost", True)
    cadence_window.resizable(True, True)
    cadence_window.wm_attributes('-transparent',True) #both label and window must be configured for transparency to work
    # Add content to the window 
    cadence_value = tk.IntVar(cadence_window,value=0)  # Initialize with value 0
    handler.cadence_value = cadence_value  # Set the handler's cadence value

    cadence_label = tk.Label(cadence_window, textvariable=cadence_value, bg="systemTransparent", font=("Arial", 20),bd=0)
    cadence_label.pack(expand=True, fill="both")
    update_cadence_value()  # Start updating the power value
    #adjust text size
    cadence_window.bind("<Configure>", adjust_text_size)
    adjust_text_size()
     # start new ant connection in new thread
    # power_value = tk.IntVar(cadence_window,value=0)
    # start_ant(node,PowerMeter, on_cadence_data)



# Main window setup (acts as the base)
main_window = tk.Tk()
main_window.withdraw()  # Hide the main window since it's not needed

# Initialize the node and set the network key
node = Node()
node.set_network_key(0x00, ANTPLUS_NETWORK_KEY)
# Create an instance of the data handler
handler = AntDataHandler()

# Create the first and second windows right away
# create_heart_rate_window(node)
create_heart_rate_window(handler)
create_power_window(handler)
create_cadence_window(handler)
# Start the ANT+ process for HeartRate and PowerMeter after windows are created
start_ant(node, HeartRate, handler.on_heart_rate_data)
start_ant(node, PowerMeter, handler.on_power_data)
# Start the Tkinter event loop
main_window.mainloop()