import tkinter as tk

class FontSizeAdjuster:
    def __init__(self, root):
        self.root = root
        self.root.title("Font Size Adjuster")

        # Create a Label to display text with adjustable font size
        self.label = tk.Label(root, text="Python-Tkinter", font=("Helvetica", 12))
        self.label.pack(pady=20)

        # Create a Scale widget to adjust font size
        self.scale = tk.Scale(root, from_=8, to=36, orient=tk.HORIZONTAL, label="Font Size",
                              command=self.update_font_size)
        self.scale.pack()

    def update_font_size(self, font_size):
        # Update the font size of the Label text
        self.label.config(font=("Helvetica", int(font_size)))

if __name__ == "__main__":
    root = tk.Tk()
    app = FontSizeAdjuster(root)
    root.mainloop()
