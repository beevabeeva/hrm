# trying to figure out setting focus for multiple tk windows when there is no standard title bar on them
# this is based on https://www.codewithfaraz.com/python/40/create-sticky-notes-in-python-using-tkinter
# but we use Canvas instead of Frame so we can put text in our custom title bar.

import tkinter as tk
from tkinter import Toplevel, Canvas, Frame, X, TOP, Label, RIGHT, LEFT, BOTH, Tk, Menu, font, filedialog, colorchooser, simpledialog, messagebox
import tkinter.scrolledtext as tkst


no_of_windows = 1
notes = []  # to keep track of all open notes

class StickyNotes(Toplevel):
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.wm_attributes("-topmost", True) # have to do this here before anything else, otherwise it does not work (i.e window does not stay on top of all others)
        self.xclick = 0
        self.yclick = 0
        self.minimized = False
        self.filepath = None

        # master (root) window
        self.overrideredirect(True)
        global no_of_windows
        # self.geometry('350x450+' + str(1000 + no_of_windows * (-30)) + '+' + str(100 + no_of_windows * 20))
        self.config(bg='#838383')
        self.attributes('-topmost', 'true')
        self.resizable(True, True)
        # self.custom_font = font.Font(size=15)


       
        #set font for title bar and get length so we can place text in the centre (no better way unfortunately)
        self.custom_font = font.Font(family="Helvetica", size="15", weight="bold")
        self.text_width = self.custom_font.measure("Heart Rate")
        # titlebar
        self.titlebar = Canvas(self, bg='yellow', relief='flat', bd=0)
        self.titlebar.bind('<Button-1>', self.get_pos)
        self.titlebar.bind('<B1-Motion>', self.move_window)
        #get text centre coord x = (rect_w - text_w)/2;
        self.x_cent = self.titlebar.winfo_reqwidth() #+ self.text_width
        self.titlebar.create_text(0, 10, text="Heart Rate", fill="black", font=('Helvetica 15 bold'), anchor='ne')
        self.titlebar.pack(fill="both", expand=1, side=TOP)
        print(self.geometry())
        print(self.titlebar.winfo_reqwidth())

        print(self.text_width)



        # self.closebutton = Label(self.titlebar, text='X', bg='#FFFF00', relief='flat', cursor='hand2', font=self.custom_font)
        # self.closebutton.bind('<Button-1>', self.quit_window)
        # self.closebutton.pack(side=RIGHT)

        # self.minbutton = Label(self.titlebar, text='-', bg='#FFFF00', relief='flat', cursor='hand2', width=2, font=self.custom_font)
        # self.minbutton.bind('<Button-1>', self.minimize_window)
        # self.minbutton.pack(side=RIGHT)

        self.newbutton = Label(self.titlebar, text='+', bg='#FFFF00', relief='flat', cursor='hand2', font=self.custom_font)
        self.newbutton.pack(side=LEFT)
        self.newbutton.bind('<Button-1>', self.another_window)

        self.mainarea = tkst.ScrolledText(self, bg='#FFFFA5', font=('Comic Sans MS', 14, 'italic'), relief='flat', padx=5, pady=10)
        self.mainarea.pack(fill=BOTH, expand=1)

        self.mainarea.bind("<Button-3>", self.show_context_menu)

        no_of_windows += 1
        notes.append(self)


    def get_pos(self, event):
        self.xclick = event.x
        self.yclick = event.y

    def move_window(self, event):
        self.geometry('+{0}+{1}'.format(event.x_root - self.xclick, event.y_root - self.yclick))

    def another_window(self, event):
        StickyNotes(root)


    def quit_window(self, event):
        self.closebutton.config(relief='flat', bd=0)
        if messagebox.askyesno('Delete Note?', 'Are you sure you want to delete this note?', parent=self):
            global no_of_windows
            self.destroy()
            notes.remove(self)
            no_of_windows -= 1
            if no_of_windows == 1:
                root.destroy()
            return
        self.closebutton.config(relief='flat', bd=0, bg='#F8F7B6')

    def minimize_window(self, event):
        if not self.minimized:
            self.mainarea.pack_forget()
            self.geometry("350x30")
            self.minimized = True
        else:
            self.mainarea.pack(fill=BOTH, expand=1)
            self.geometry("350x450")
            self.minimized = False

    def show_context_menu(self, event):
        context_menu = Menu(self, tearoff=0)
        context_menu.add_command(label="Change Font", command=self.change_font)
        context_menu.add_command(label="Change Background Color", command=self.change_bg_color)
        context_menu.add_command(label="Save Note", command=self.save_note)
        context_menu.add_command(label="Load Note", command=self.load_note)
        context_menu.post(event.x_root, event.y_root)

    def change_font(self):
        new_font = simpledialog.askstring("Change Font", "Enter font (e.g., Arial, 12):")
        if new_font:
            font_name, font_size = new_font.split(',')
            self.mainarea.config(font=(font_name.strip(), int(font_size.strip())))

    def change_bg_color(self):
        color = colorchooser.askcolor()[1]
        if color:
            self.mainarea.config(bg=color)

    def save_note(self):
        if self.filepath is None:
            self.filepath = filedialog.asksaveasfilename(defaultextension=".txt",
                                                         filetypes=[("Text files", "*.txt"), ("All files", "*.*")])
        if self.filepath:
            with open(self.filepath, "w") as f:
                f.write(self.mainarea.get("1.0", tk.END))

    def load_note(self):
        self.filepath = filedialog.askopenfilename(filetypes=[("Text files", "*.txt"), ("All files", "*.*")])
        if self.filepath:
            with open(self.filepath, "r") as f:
                self.mainarea.delete("1.0", tk.END)
                self.mainarea.insert("1.0", f.read())

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            global no_of_windows
            no_of_windows -= 1
            self.destroy()
            if no_of_windows == 1:
                root.destroy()


root = Tk()
root.withdraw()



def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        for note in notes:
            note.on_closing()
        root.destroy()

root.protocol("WM_DELETE_WINDOW", on_closing)

sticky = StickyNotes(root)

root.mainloop()