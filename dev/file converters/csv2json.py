import csv
import json
import datetime

def convert_csv_to_json(csv_file, json_file):
    data = []
    
    with open(csv_file, 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            # Convert the timestamp to ISO format
            timestamp_iso = datetime.datetime.strptime(row['timestamp'], '%Y-%m-%d %H:%M:%S').isoformat() + 'Z'
            
            # Create a dictionary for each record
            record = {
                "timestamp": timestamp_iso,
                "heart_rate": int(row['heart_rate']),
                "power": int(row['power']),
                "cadence": int(row['cadence']),
                "speed": float(row['speed']),
                "distance": float(row['distance'])
            }
            data.append(record)
    
    # Write data to a JSON file
    with open(json_file, 'w') as outfile:
        json.dump(data, outfile, indent=4)

# Example usage
convert_csv_to_json('workouts/workout_20240817_120940.csv', 'output.json')
