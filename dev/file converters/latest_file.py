import os
import glob

def get_latest_file(directory, extension='*'):
    # Construct the search pattern with the given directory and file extension
    search_pattern = os.path.join(directory, f'*.{extension}')
    
    # Get the list of all files matching the pattern
    files = glob.glob(search_pattern)
    
    # Check if there are any files found
    if not files:
        return None
    
    # Find the latest file based on the last modified time
    latest_file = max(files, key=os.path.getmtime)
    print(latest_file)
    return latest_file

get_latest_file("workouts/csv","csv")