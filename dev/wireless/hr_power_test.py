import tkinter as tk
from tkinter import messagebox
from tkinter import simpledialog
import time
# from tkmacosx import Button # didn't solve ghosting issue

import asyncio
from bleak import BleakClient

from pycycling.heart_rate_service import HeartRateService
from threading import Thread


class UpdateAgeDialog(tk.Toplevel):
    def __init__(self, current_age, current_zones, app_instance):
        super().__init__()

        self.current_age = current_age
        self.new_age = current_age
        self.current_zones = current_zones
        self.app_instance = app_instance

        self.title("Update Age and Zones")

        # Current settings label
        self.current_settings_label = tk.Label(self, text="Current Settings:")
        self.current_settings_label.grid(row=0, column=0, columnspan=2, padx=10, pady=5)

        self.current_age_label = tk.Label(self, text=f"Age: {self.current_age}")
        self.current_age_label.grid(row=1, column=0, padx=10, pady=5)

        self.current_zones_label = tk.Label(self, text="Zones:")
        self.current_zones_label.grid(row=1, column=1, padx=10, pady=5)

        row = 2
        for zone, values in self.current_zones.items():
            lower, upper = values
            self.zone_label = tk.Label(self, text=f"{zone}: {int(lower)} - {int(upper)} bpm")
            self.zone_label.grid(row=row, column=1, padx=10, pady=2, sticky='w')
            row += 1

        # Entry field for new age
        self.new_age_label = tk.Label(self, text="New Age:")
        self.new_age_label.grid(row=row, column=0, padx=10, pady=5)

        self.new_age_entry = tk.Entry(self)
        self.new_age_entry.grid(row=row, column=1, padx=10, pady=5)
                
        row += 1

        # Buttons
        self.update_button = tk.Button(self, text="Update",command=lambda: self.update_age(False))
        self.update_button.grid(row=row, column=0, padx=10, pady=5)
        self.update_button.bind("<Button-1>", self.update_age)  # weird double click behaviour without this bind.

        self.ok_button = tk.Button(self, text="OK", command=self.ok)
        self.ok_button.grid(row=row, column=1, padx=10, pady=5)

        self.cancel_button = tk.Button(self, text="Cancel", command=self.cancel)
        self.cancel_button.grid(row=row, column=2, padx=10, pady=5)

        self.new_age_entry.focus()
        self.result = None

    def update_age(self, event=None):
        new_age = self.new_age_entry.get()
        print("new age:", new_age) #debug
        self.current_age_label.config(text=f"Age: {self.new_age}")
        self.current_age_label.grid(row=1, column=0, padx=10, pady=5)

        self.max_heart_rate = 220 - self.new_age
        self.hr_zones = {
            "Maximum VO2 Max Zone": (0.9 * self.max_heart_rate, self.max_heart_rate),
            "Hard Anaerobic Zone": (0.8 * self.max_heart_rate, 0.9 * self.max_heart_rate),
            "Moderate Aerobic Zone": (0.7 * self.max_heart_rate, 0.8 * self.max_heart_rate),
            "Light Fat Burn Zone": (0.6 * self.max_heart_rate, 0.7 * self.max_heart_rate),
            "Very Light Warm Up Zone": (0.5 * self.max_heart_rate, 0.6 * self.max_heart_rate)
        }

        row = 2
        for zone, values in self.hr_zones.items():
            lower, upper = values
            self.zone_label.config(text=f"{zone}: {int(lower)} - {int(upper)} bpm")
            self.zone_label.grid(row=row, column=1, padx=10, pady=2, sticky='w')
            row += 1
        if new_age.isdigit() and 0 < int(new_age) < 150:
            self.new_age = int(new_age)
            self.new_age_entry.delete(0, tk.END)
            self.new_age_entry.insert(0, str(self.new_age))
            self.app_instance.age = self.new_age  # Update age in the main app instance
            self.app_instance.save_age(self.new_age)  # Save new age to text file


    def ok(self):
        self.result = "OK"
        self.app_instance.age = self.new_age  # Update age in the main app instance
        self.app_instance.save_age(self.new_age)  # Save new age to text file
        self.destroy()

    def cancel(self):
        self.result = "Cancel"
        self.destroy()

async def hrm_data(address):
    async with BleakClient(address) as client:
        def my_measurement_handler(data):
            print(data)
            try:
                heart_rate = data.bpm
                app.heart_rate_value.set(str(heart_rate))
            except Exception as e:
                print("Error parsing heart rate data:", e)           

        await client.is_connected()
        hr_service = HeartRateService(client)
        hr_service.set_hr_measurement_handler(my_measurement_handler)

        await hr_service.enable_hr_measurement_notifications()
        await asyncio.sleep(10800.0)
        # await hr_service.disable_hr_measurement_notifications()


class HeartRateMonitorApp:
    def __init__(self, master):
        self.master = master
        #window stuff:
        master.overrideredirect(True)  # Remove system toolbar
        master.title("Heart Rate Monitor")
        master.resizable(True, True)
        master.bind("<Configure>", self.adjust_text_size)
        master.wm_attributes('-transparent',True)
        master.minsize(width= master.winfo_screenwidth() //10, height=master.winfo_screenheight()// 10)

        # Create Fake Title Bar
        # self.title_bar = tk.Frame(master, bg="darkgreen", relief="raised", bd=0)
        # Create buttons for minimize, maximize, and close
        # self.close_button = tk.Button(self.title_bar, text="✕", command=master.destroy)
        # Pack the buttons into the title bar frame
        # self.close_button.pack(side=tk.LEFT, padx=2)
        # self.title_bar.pack(expand=1, fill="x")
        # Bind the titlebar
        self.master.bind("<ButtonPress-1>", lambda event: event.widget.focus_set())
        self.master.bind("<ButtonPress-1>", self.oldxyset)
        self.master.bind("<B1-Motion>", self.move_app)
        # Create title text
        # self.title_label = tk.Label(self.title_bar, text="", bg="darkgreen", fg="white")
        # self.title_label.pack(side="left", pady=4)

        # Create menu bar
        menubar = tk.Menu(master)
        master.config(menu=menubar)

        # Create Settings menu
        settings_menu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="Settings", menu=settings_menu)
        settings_menu.add_command(label="Set Age", command=self.set_age)
        # Load user's age from file or set default age
        self.age = self.load_age() or 30
        print("age: ",self.age) #debug

        self.label = tk.Label(master, text="Heart Rate:", font=("Helvetica", 16))
        self.label.pack()

        self.heart_rate_value = tk.StringVar()
        self.heart_rate_value.set("000")  # Set initial value to zero
        self.heart_rate_label = tk.Label(master, textvariable=self.heart_rate_value, font=("Helvetica", 10),bg='systemTransparent',bd=0)#state='normal')#activeforeground="red")#disabledforeground='systemTransparent',highlightbackground='systemTransparent',highlightcolor='systemTransparent')
        self.heart_rate_label.pack(expand=True, fill="both")

        self.update_heart_rate()
  
        master.wm_attributes("-topmost", True) # tried hacking mac with extensions, does not work. have to do this to keep window on top so I can watch things. must come after transparency


        self.title_bar_visible = False
  

        # Bind click event to toggle visibility of the title bar
        # self.master.bind("<Button-1>", self.toggle_title_bar2) #causes weird ghosting issue. abandoning for now
        master.wm_attributes("-topmost", True)



    def set_age(self):
        self.max_heart_rate = 220 - self.age
        self.hr_zones = {
            "Maximum VO2 Max Zone": (0.9 * self.max_heart_rate, self.max_heart_rate),
            "Hard Anaerobic Zone": (0.8 * self.max_heart_rate, 0.9 * self.max_heart_rate),
            "Moderate Aerobic Zone": (0.7 * self.max_heart_rate, 0.8 * self.max_heart_rate),
            "Light Fat Burn Zone": (0.6 * self.max_heart_rate, 0.7 * self.max_heart_rate),
            "Very Light Warm Up Zone": (0.5 * self.max_heart_rate, 0.6 * self.max_heart_rate)
        }
        zone_labels = []
        for zone, (lower, upper) in self.hr_zones.items():
            label_text = f"{zone}: {int(lower)} - {int(upper)} bpm"
            zone_labels.append(label_text)

        update_dialog = UpdateAgeDialog(self.age, self.hr_zones,self)
        self.master.wait_window(update_dialog)


    def load_age(self):
        try:
            with open("user_age.txt", "r") as file:
                return int(file.read().strip())
        except FileNotFoundError:
            return None

    def save_age(self, age):
        with open("user_age.txt", "w") as file:
            file.write(str(age))


    def adjust_text_size(self,event):
        current_font_size=int(self.heart_rate_label.cget("font").split()[1])
        print("font_size",current_font_size)
        new_width = event.width
        new_height = event.height
        font_percentage = 0.7  
        # Calculate the new font size based on the window size
        # window_ratio = min(new_width // 2, new_height // 2)
        new_font_size = int(min(new_width, new_height) * font_percentage)

        # Set the new font size for the label
        self.heart_rate_label.config(font=("Helvetica", new_font_size,"bold"),padx=0, pady=0)


    def toggle_title_bar2(self, event):
        # Toggle visibility of the custom title bar
        if self.title_bar.winfo_ismapped():
            self.title_bar.pack_forget()  # Hide the title bar
        else:
            self.title_bar.pack(fill="x")  # Show the title bar

    def toggle_title_bar(self, event):
        # Toggle visibility of the title bar
        self.heart_rate_label.config(bg="red")
        if self.title_bar_visible:
            # pass
            self.master.wm_attributes("-topmost", True)
        else:
            # pass
            self.master.wm_attributes("-topmost", True)
        self.title_bar_visible = not self.title_bar_visible
        self.master.wm_attributes("-topmost", True)
    
    def oldxyset(self, event):
        self.oldx = event.x 
        self.oldy = event.y
    
    def move_app(self,event):
        self.y = event.y_root - self.oldy
        self.x = event.x_root - self.oldx
        self.master.geometry(f'+{self.x}+{self.y}')


    def update_heart_rate(self):
        # fetch heart rate data
        current_rate = int(self.heart_rate_value.get()) if self.heart_rate_value.get() else 0
        # self.heart_rate_value.set(str(current_rate + 5)) #debug
        print(self.heart_rate_value.get()) #debug
        # self.heart_rate_label = tk.Label(self.master, textvariable=self.heart_rate_value, font=("Helvetica", 140),bg='systemTransparent')#state='normal')#activeforeground="red")#disabledforeground='systemTransparent',highlightbackground='systemTransparent',highlightcolor='systemTransparent')
        # teper = self.load_age()
        # print(teper)
        # Change text color based on heart rate
        heart_rate = int(self.heart_rate_value.get())
        self.heart_rate_label.config(bg="systemTransparent")

        # Calculate heart rate zones based on maximum heart rate
        max_hr = 220 - self.age
        hr_zones = {
            "Maximum VO2 Max Zone": (0.9 * max_hr, max_hr),
            "Hard Anaerobic Zone": (0.8 * max_hr, 0.9 * max_hr),
            "Moderate Aerobic Zone": (0.7 * max_hr, 0.8 * max_hr),
            "Light Fat Burn Zone": (0.6 * max_hr, 0.7 * max_hr),
            "Very Light Warm Up Zone": (0.5 * max_hr, 0.6 * max_hr)
        }

        # if 170 <= heart_rate:
        #     self.heart_rate_label.config(fg="red")
        # elif 151 <= heart_rate < 170:
        #     self.heart_rate_label.config(fg="orange")
        # elif 132 <= heart_rate < 151:
        #     self.heart_rate_label.config(fg="yellow")
        # elif 113 <= heart_rate < 132:
        #     self.heart_rate_label.config(fg="green")
        # else:
        #     self.heart_rate_label.config(fg="blue")
        # Determine the color based on the heart rate zone
        color = "blue"  # Default color
        for zone, (lower, upper) in hr_zones.items():
            if heart_rate > upper:
                color = "red"
            elif lower <= heart_rate < upper:
                if zone == "Maximum VO2 Max Zone":
                    color = "red"
                elif zone == "Hard Anaerobic Zone":
                    color = "orange"
                elif zone == "Moderate Aerobic Zone":
                    color = "yellow"
                elif zone == "Light Fat Burn Zone":
                    color = "green"
                break

        # Update the heart rate label color
        self.heart_rate_label.config(fg=color)#,bg="systemTransparent",disabledforeground='systemTransparent',highlightbackground='systemTransparent',highlightcolor='systemTransparent')

        # Set the background back to "systemTransparent"
        # self.heart_rate_label.config(bg="red")
        self.master.after(1000, self.update_heart_rate)
        
class PowerWindow:
    def __init__(self, master):
        self.master = master
        master.overrideredirect(True)
        master.title("Power Monitor")
        master.resizable(True, True)
        master.bind("<Configure>", self.adjust_text_size)
        master.wm_attributes('-transparent', True)
        master.minsize(width=master.winfo_screenwidth() // 10, height=master.winfo_screenheight() // 10)

        self.master.bind("<ButtonPress-2>", self.oldxyset2)
        self.master.bind("<B2-Motion>", self.move_app2)

        self.label = tk.Label(master, text="Power:", font=("Helvetica", 16))
        self.label.pack()

        self.power_value = tk.StringVar()
        self.power_value.set("000")
        self.power_label = tk.Label(master, textvariable=self.power_value, font=("Helvetica", 10), bg='systemTransparent', bd=0)
        self.power_label.pack(expand=True, fill="both")

        self.update_power()

        master.wm_attributes("-topmost", True) # this doesn't seem to do anything. if I click on a non-tk window, the power window disappears but hr is still on top.

    def adjust_text_size(self, event):
        new_width = event.width
        new_height = event.height
        font_percentage = 0.7
        new_font_size = int(min(new_width, new_height) * font_percentage)
        self.power_label.config(font=("Helvetica", new_font_size, "bold"), padx=0, pady=0)

    def oldxyset2(self, event):
        self.oldx = event.x 
        self.oldy = event.y
    
    def move_app2(self, event):
        x = self.master.winfo_x() + (event.x_root - self.master.winfo_rootx()) - self.oldx
        y = self.master.winfo_y() + (event.y_root - self.master.winfo_rooty()) - self.oldy
        self.master.geometry(f'+{x}+{y}')

    def update_power(self):
        # Here you would update the power value
        # For now, let's just increment it for demonstration
        current_power = int(self.power_value.get()) if self.power_value.get() else 0
        self.power_value.set(str((current_power + 5) % 1000))
        
        self.master.after(1000, self.update_power)

import os

os.environ["PYTHONASYNCIODEBUG"] = str(1)

def run_hrm_data():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(hrm_data(device_address))

# BLE device address
device_address = "F1982529-2BF7-810D-ED72-372360D681F3"

# Start the Tkinter application in a new thread
thread = Thread(target=run_hrm_data)
thread.daemon = True
thread.start()

# Create the main root window
root = tk.Tk()
app = HeartRateMonitorApp(root)

# Create a separate root window for the power display
power_root = tk.Toplevel()
power_app = PowerWindow(power_root)

# Position the windows
root.geometry("+50+50")
power_root.geometry("+300+50")

root.mainloop()
