import asyncio
from bleak import BleakClient, BleakScanner
import time

# Replace this with your device's address
device_address = "F1982529-2BF7-810D-ED72-372360D681F3"

async def main():
    # Discover devices
    print("Starting discovery...")
    start_discovery_time = time.time()

    devices = await BleakScanner.discover()
    discovered_device = None

    for device in devices:
        if device.address == device_address:
            discovered_device = device
            break

    discovery_duration = time.time() - start_discovery_time

    if discovered_device:
        print(f"Device {device_address} discovered in {discovery_duration:.2f} seconds.")
        
        # Connect to the device
        print("Connecting to the device...")
        start_connection_time = time.time()

        async with BleakClient(device_address) as client:
            connection_duration = time.time() - start_connection_time
            if client.is_connected:
                print(f"Connected to the device in {connection_duration:.2f} seconds.")
            else:
                print("Failed to connect to the device.")
    else:
        print(f"Device {device_address} not found during discovery.")

# Run the main function
asyncio.run(main())
