import asyncio
from bleak import BleakClient, BleakScanner
import time

# Replace these with your device addresses
device_addresses = [
    "F1982529-2BF7-810D-ED72-372360D681F3",
    "0E0DB68C-0E0A-2490-11A1-2A4F4925151C"
]

async def connect_to_device(address):
    # Discover the device
    print(f"Starting discovery for {address}...")
    start_discovery_time = time.time()

    devices = await BleakScanner.discover()
    discovered_device = None

    for device in devices:
        if device.address == address:
            discovered_device = device
            break

    discovery_duration = time.time() - start_discovery_time

    if discovered_device:
        print(f"Device {address} discovered in {discovery_duration:.2f} seconds.")
        
        # Connect to the device
        print(f"Connecting to {address}...")
        start_connection_time = time.time()

        async with BleakClient(address) as client:
            connection_duration = time.time() - start_connection_time
            if client.is_connected:
                print(f"Connected to {address} in {connection_duration:.2f} seconds.")
            else:
                print(f"Failed to connect to {address}.")
    else:
        print(f"Device {address} not found during discovery.")

async def main():
    # Connect to both devices sequentially
    for address in device_addresses:
        await connect_to_device(address)

# Run the main function
asyncio.run(main())
