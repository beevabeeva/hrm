import asyncio
from bleak import BleakClient, BleakScanner
import time

# Replace these with your device addresses
device_addresses = [
    "F1982529-2BF7-810D-ED72-372360D681F3",
    "0E0DB68C-0E0A-2490-11A1-2A4F4925151C"
]

async def connect_to_device(address, discovery_timeout):
    # Discover the device
    print(f"Starting discovery for {address}...")
    start_discovery_time = time.time()

    devices = await BleakScanner.discover(timeout=discovery_timeout)
    discovered_device = None

    for device in devices:
        if device.address == address:
            discovered_device = device
            break

    discovery_duration = time.time() - start_discovery_time

    if discovered_device:
        print(f"Device {address} discovered in {discovery_duration:.2f} seconds.")
        
        # Connect to the device
        print(f"Connecting to {address}...")
        start_connection_time = time.time()

        async with BleakClient(address) as client:
            connection_duration = time.time() - start_connection_time
            if client.is_connected:
                print(f"Connected to {address} in {connection_duration:.2f} seconds.")
            else:
                print(f"Failed to connect to {address}.")
    else:
        print(f"Device {address} not found during discovery.")

    return discovery_duration + connection_duration if discovered_device else discovery_duration

async def main():
    total_time_start = time.time()

    total_duration = 0
    for address in device_addresses:
        duration = await connect_to_device(address, discovery_timeout=2)  # Increased discovery timeout to 30 seconds
        total_duration += duration

    total_time_end = time.time()
    total_time_taken = total_time_end - total_time_start

    print(f"Total time taken for discovery and connection: {total_duration:.2f} seconds")
    print(f"Total elapsed time for the entire process: {total_time_taken:.2f} seconds")

# Run the main function
asyncio.run(main())
