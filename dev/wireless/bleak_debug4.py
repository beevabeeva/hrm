import asyncio
from bleak import BleakScanner, BleakClient
import time

async def discover_device(address, retry_attempts=10, timeout=1):
    """Discover a specific Bluetooth device with retries."""
    for attempt in range(retry_attempts):
        print(f"Discovery attempt {attempt + 1} for {address}")
        devices = await BleakScanner.discover(timeout=timeout)
        for device in devices:
            if device.address == address:
                print(f"Found device {address}")
                return device
        print(f"Device {address} not found in this attempt. Retrying...")
        await asyncio.sleep(1)  # Wait a bit before retrying
    raise Exception(f"Device {address} not found after {retry_attempts} attempts.")

async def connect_device(address):
    """Connect to a Bluetooth device and measure connection time."""
    start_time = time.time()
    try:
        device = await discover_device(address)
        async with BleakClient(device) as client:
            end_time = time.time()
            print(f"Connected to {address} in {end_time - start_time:.2f} seconds.")
            # Perform operations with the client here
            # await asyncio.sleep(10)  # Keep the connection open for 10 seconds
    except Exception as e:
        print(f"Error connecting to {address}: {e}")
        return None, None
    return start_time, end_time

async def main():
    address1 = "F1982529-2BF7-810D-ED72-372360D681F3"
    address2 = "0E0DB68C-0E0A-2490-11A1-2A4F4925151C"
    
    start_time = time.time()
    results = await asyncio.gather(
        connect_device(address1),
        connect_device(address2)
    )
    end_time = time.time()

    # Calculate total time
    total_discovery_and_connection_time = end_time - start_time
    print(f"Total time for discovering and connecting to both devices: {total_discovery_and_connection_time:.2f} seconds")

if __name__ == "__main__":
    asyncio.run(main())
