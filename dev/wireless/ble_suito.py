# import asyncio
# from bleak import BleakClient

# from pycycling.cycling_power_service import CyclingPowerService

# async def run(address):
#     async with BleakClient(address) as client:
#         def my_measurement_handler(data):
#             print(data)

#         await client.is_connected()
#         trainer = CyclingPowerService(client)
#         trainer.set_cycling_power_measurement_handler(my_measurement_handler)
#         await trainer.enable_cycling_power_measurement_notifications()
#         await asyncio.sleep(30.0)
#         await trainer.disable_cycling_power_measurement_notifications()


# if __name__ == "__main__":
#     import os

#     os.environ["PYTHONASYNCIODEBUG"] = str(1)
# # 0E0DB68C-0E0A-2490-11A1-2A4F4925151C: SUITO
#     device_address = "0E0DB68C-0E0A-2490-11A1-2A4F4925151C"
#     loop = asyncio.get_event_loop()
#     loop.run_until_complete(run(device_address))


import asyncio
from bleak import BleakClient
from pycycling.cycling_power_service import CyclingPowerService

async def run(address):
    async with BleakClient(address) as client:
        def my_measurement_handler(data):
            print(data)

        await client.is_connected()
        trainer = CyclingPowerService(client)
        trainer.set_cycling_power_measurement_handler(my_measurement_handler)
        await trainer.enable_cycling_power_measurement_notifications()
        
        try:
            # Run indefinitely
            while True:
                await asyncio.sleep(1)
        except asyncio.CancelledError:
            # Handle cancellation (e.g., when the user presses Ctrl+C)
            print("Measurement cancelled. Cleaning up...")
        finally:
            # Disable notifications before exiting
            await trainer.disable_cycling_power_measurement_notifications()

if __name__ == "__main__":
    import os

    os.environ["PYTHONASYNCIODEBUG"] = str(1)
    # 0E0DB68C-0E0A-2490-11A1-2A4F4925151C: SUITO
    device_address = "0E0DB68C-0E0A-2490-11A1-2A4F4925151C"
    
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(run(device_address))
    except KeyboardInterrupt:
        print("Program interrupted by user. Exiting...")
    finally:
        loop.close()