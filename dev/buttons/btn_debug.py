import tkinter as tk

class SimpleButton:
    def __init__(self, canvas, side, command=None, text=None):
        self.canvas = canvas
        self.command = command
        self.side = side
        self.text = text
        
        # Ensure canvas size is updated before creating button
        self.canvas.update_idletasks()
        self.create_button()

    def create_button(self):
        canvas_width = self.canvas.winfo_width()
        canvas_height = self.canvas.winfo_height()

        # Ensure canvas dimensions are valid
        if canvas_width <= 0 or canvas_height <= 0:
            raise ValueError("Canvas width and height must be positive")

        # Determine the button position and size
        if self.side == "left":
            x1, x2 = 0, canvas_width // 4
        elif self.side == "right":
            x1, x2 = canvas_width - canvas_width // 4, canvas_width
        else:
            raise ValueError("Side must be 'left' or 'right'")
        
        y1, y2 = 0, canvas_height

        # Create the button rectangle and text
        self.rect_id = self.canvas.create_rectangle(x1, y1, x2, y2, fill="white", outline="black")
        self.text_id = self.canvas.create_text((x1 + x2) // 2, (y1 + y2) // 2, text=self.text, fill="black", font=("Arial", 12))

        # Bind click event
        self.canvas.tag_bind(self.rect_id, "<Button-1>", self.on_click)
        self.canvas.tag_bind(self.text_id, "<Button-1>", self.on_click)

    def on_click(self, event):
        if self.command:
            print(self.command)
            self.command()

# Example usage
def left_button_click():
    print("LEFT Button clicked")

def right_button_click():
    print("RIGHT Button clicked")

root = tk.Tk()
canvas = tk.Canvas(root, width=400, height=200, bg="lightgray")
canvas.pack()

# Create buttons after packing the canvas
left_button = SimpleButton(canvas, side="left", command=left_button_click, text="Left Button")
right_button = SimpleButton(canvas, side="right", command=right_button_click, text="Right Button")

root.mainloop()
