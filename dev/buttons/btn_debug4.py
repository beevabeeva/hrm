import tkinter as tk
from math import pi, cos, sin
from typing import Iterator

class RoundedCanvas(tk.Canvas):
    minimum_steps = 10  # lower values give pixelated corners

    @staticmethod
    def get_cos_sin(radius: int) -> Iterator[tuple[float, float]]:
        steps = max(radius, RoundedCanvas.minimum_steps)
        for i in range(steps + 1):
            angle = pi * (i / steps) * 0.5
            yield (cos(angle) - 1) * radius, (sin(angle) - 1) * radius

    def create_rounded_box(self, x0: int, y0: int, x1: int, y1: int, radius: int, color: str) -> int:
        points = []
        cos_sin_r = tuple(self.get_cos_sin(radius))
        for cos_r, sin_r in cos_sin_r:
            points.append((x1 + sin_r, y0 - cos_r))
        for cos_r, sin_r in cos_sin_r:
            points.append((x1 + cos_r, y1 + sin_r))
        for cos_r, sin_r in cos_sin_r:
            points.append((x0 - sin_r, y1 + cos_r))
        for cos_r, sin_r in cos_sin_r:
            points.append((x0 - cos_r, y0 - sin_r))
        return self.create_polygon(points, fill=color, outline="black")

class SimpleButton:
    def __init__(self, canvas, side, command=None, textvariable=None):
        self.canvas = canvas
        self.command = command
        self.side = side
        self.textvariable = textvariable

        # Ensure canvas size is updated before creating button
        self.canvas.update_idletasks()
        self.create_button()

        # Bind the resize event to update button size and position
        self.canvas.bind("<Configure>", self.resize_button)

    def create_button(self):
        canvas_width = self.canvas.winfo_width()
        canvas_height = self.canvas.winfo_height()

        # Ensure canvas dimensions are valid
        if canvas_width <= 0 or canvas_height <= 0:
            raise ValueError("Canvas width and height must be positive")

        # Determine the button position and size
        if self.side == "left":
            x1, x2 = 0, canvas_width // 4
        elif self.side == "right":
            x1, x2 = canvas_width - canvas_width // 4, canvas_width
        else:
            raise ValueError("Side must be 'left' or 'right'")

        y1, y2 = 0, canvas_height

        # Create the button rectangle and text
        self.rect_id = self.canvas.create_rounded_box(x1, y1, x2, y2, radius=18, color="white")
        self.text_id = self.canvas.create_text((x1 + x2) // 2, (y1 + y2) // 2, text=self.textvariable.get(), fill="black", font=("Arial", 12))

        # Bind click event
        self.canvas.tag_bind(self.rect_id, "<Button-1>", self.on_click)
        self.canvas.tag_bind(self.text_id, "<Button-1>", self.on_click)

    def resize_button(self, event=None):
        # Remove the existing rectangle and text
        self.canvas.delete(self.rect_id)
        self.canvas.delete(self.text_id)

        # Recreate the button with updated dimensions
        self.create_button()

    def on_click(self, event):
        if self.command:
            self.command()

# Example usage
def left_button_click():
    left_var.set("hello")
    print("LEFT Button clicked")

def right_button_click():
    right_var.set("hello")
    print("RIGHT Button clicked")

root = tk.Tk()

# Create a single RoundedCanvas for both buttons
canvas = RoundedCanvas(root, width=400, height=100, bg="lightgray")
canvas.pack(fill=tk.BOTH, expand=True)

# Create StringVars to hold the text values
left_var = tk.StringVar(value="Left")
right_var = tk.StringVar(value="Right")

# Create buttons using the single canvas
left_button = SimpleButton(canvas, side="left", command=left_button_click, textvariable=left_var)
right_button = SimpleButton(canvas, side="right", command=right_button_click, textvariable=right_var)

# Update button text based on the StringVar value
def update_left_button_text(*args):
    left_button.canvas.itemconfig(left_button.text_id, text=left_var.get())
    
def update_right_button_text(*args):
    right_button.canvas.itemconfig(right_button.text_id, text=right_var.get())

left_var.trace("w", update_left_button_text)
right_var.trace("w", update_right_button_text)

root.mainloop()
