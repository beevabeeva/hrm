from tkinter import *

def round_rectangle(canvas, x1, y1, x2, y2, radius=25, **kwargs):
    points = [x1+radius, y1,
              x1+radius, y1,
              x2-radius, y1,
              x2-radius, y1,
              x2, y1,
              x2, y1+radius,
              x2, y1+radius,
              x2, y2-radius,
              x2, y2-radius,
              x2, y2,
              x2-radius, y2,
              x2-radius, y2,
              x1+radius, y2,
              x1+radius, y2,
              x1, y2,
              x1, y2-radius,
              x1, y2-radius,
              x1, y1+radius,
              x1, y1+radius,
              x1, y1]
    return canvas.create_polygon(points, **kwargs, smooth=True)

def on_click(event):
    canvas.itemconfig(my_rectangle, fill="gray")

def button(canvas, x1, y1, x2, y2, radius=25, **kwargs):
    global my_rectangle
    my_rectangle = round_rectangle(canvas, x1, y1, x2, y2, radius=radius, **kwargs)
    # Calculate the center of the rectangle
    center_x = (x1 + x2) // 2
    center_y = (y1 + y2) // 2
    
    # Add text in the center
    my_text=canvas.create_text(center_x, center_y, text="Start", fill="black", font=("Arial", 12, "bold"))
    canvas.tag_bind(my_rectangle, "<Button-1>", on_click)
    canvas.tag_bind(my_text, "<Button-1>", on_click)


root = Tk()
canvas = Canvas(root, width=300, height=200)
canvas.pack()

button(canvas, 50, 50, 150, 100, radius=20, fill="blue")

root.mainloop()
