import tkinter as tk

class RoundedButton(tk.Canvas):
    def __init__(self, master, text, command, **kwargs):
        super().__init__(master, width=200, height=100, bg="systemTransparent", borderwidth=0, highlightthickness=0, **kwargs)
        self.command = command
        self._text = text
        self._color = "lightgray"
        self._pressed_color = "gray"
        self._hover_color = "darkgray"
        
        # Draw the initial button
        self._draw_button()
        
        # Bind events
        self.bind("<Button-1>", self._on_click)
        self.bind("<ButtonRelease-1>", self._on_release)
        self.bind("<Enter>", self._on_hover)
        self.bind("<Leave>", self._on_leave)
    
    def _draw_button(self):
        self.delete("all")
        self.create_rounded_rectangle(10, 10, self.winfo_width()-10, self.winfo_height()-10, radius=20, fill=self._color, outline="")
        self.create_text(self.winfo_width() // 2, self.winfo_height() // 2, text=self._text, fill="black", font=("Arial", 12, "bold"))

    def _on_click(self, event):
        self._color = self._pressed_color
        self._draw_button()
    
    def _on_release(self, event):
        self._color = "lightgray"
        self._draw_button()
        if self.command:
            self.command()

    def _on_hover(self, event):
        self._color = self._hover_color
        self._draw_button()
    
    def _on_leave(self, event):
        self._color = "lightgray"
        self._draw_button()

    def create_rounded_rectangle(self, x1, y1, x2, y2, radius=25, **kwargs):
        points = [x1+radius, y1,
                  x1+radius, y1,
                  x2-radius, y1,
                  x2-radius, y1,
                  x2, y1,
                  x2, y1+radius,
                  x2, y1+radius,
                  x2, y2-radius,
                  x2, y2-radius,
                  x2, y2,
                  x2-radius, y2,
                  x2-radius, y2,
                  x1+radius, y2,
                  x1+radius, y2,
                  x1, y2,
                  x1, y2-radius,
                  x1, y2-radius,
                  x1, y1+radius,
                  x1, y1+radius,
                  x1, y1]
        return self.create_polygon(points, **kwargs, smooth=True)

def toggle_start_pause():
    print("Button clicked!")

root = tk.Tk()
root.geometry("300x200")

button = RoundedButton(root, text="Start", command=toggle_start_pause)
button.pack(pady=20)

root.mainloop()
