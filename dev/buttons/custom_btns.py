import asyncio
import tkinter as tk
from bleak import BleakClient
from pycycling.fitness_machine_service import FitnessMachineService
from pycycling.heart_rate_service import HeartRateService
import threading
import os
import json
import time
import csv
from datetime import datetime, timedelta

LAYOUT_FILE = "window_layout.json"

def save_layout(windows):
    layout = {}
    for title, window in windows.items():
        geometry = window.geometry()
        font_size = window.winfo_children()[1].cget("font").split()[1]  # Get font size from label
        layout[title] = {"geometry": geometry, "font_size": font_size}
    
    with open(LAYOUT_FILE, "w") as f:
        json.dump(layout, f)
    print("Layout saved successfully!")


def load_layout():
    if os.path.exists(LAYOUT_FILE):
        with open(LAYOUT_FILE, "r") as f:
            return json.load(f)
    return None


class BluetoothDataHandler:
    def __init__(self):
        self.heart_rate_value = None
        self.power_value = None
        self.cadence_value = None
        self.speed_value = None
        self.distance_value = None
        self.timer_value = None
        self.timer_running = False
        self.start_time = 0
        self.elapsed_time = 0
        self.total_distance = 0
        self.csv_file = None
        self.csv_writer = None
        self.start_datetime = None
        self.distance_value = None
        self.total_distance = 0
        self.last_update_time = None
        

        

    def update_indoor_bike_data(self, data):
        current_time = time.time()
        # Update distance
        if self.timer_running:  # Check if the timer is running
            if self.last_update_time is not None:
                time_diff = current_time - self.last_update_time
                self.total_distance += (data.instant_speed / 3.6) * time_diff  # Convert km/h to m/s

            self.last_update_time = current_time

            if self.distance_value is not None:
                self.distance_value.set(f"{self.total_distance / 1000:.2f}")  # Display in km

        if self.power_value is not None:
            self.power_value.set(data.instant_power)
        if self.cadence_value is not None:
            self.cadence_value.set(data.instant_cadence)
        if self.speed_value is not None:
            self.speed_value.set(data.instant_speed)
        if self.distance_value is not None:
            self.distance_value.set(f"{self.total_distance / 1000:.2f}")  # Display in km
          
        # Log data to CSV file
        if self.csv_writer and self.timer_running:
            self.csv_writer.writerow({
                'timestamp': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                'heart_rate': self.heart_rate_value.get() if self.heart_rate_value else '',
                'power': self.power_value.get() if self.power_value else '',
                'cadence': self.cadence_value.get() if self.cadence_value else '',
                'speed': self.speed_value.get() if self.speed_value else '',
                'distance': self.total_distance
            })

    def update_heart_rate(self, data):
        if self.heart_rate_value is not None:
            self.heart_rate_value.set(data.bpm)

    def start_timer(self):
        current_time = time.time()
        if not self.timer_running:
            self.timer_running = True
            self.start_time = current_time - self.elapsed_time
            self.last_update_time = current_time  # Set last update time to current time to avoid jumps
            self.update_timer()
            if not self.csv_file:
                self.start_csv_logging()

    def pause_timer(self):
        if self.timer_running:
            self.timer_running = False
            self.elapsed_time = time.time() - self.start_time
            self.last_update_time = None  # Reset last update time when paused

    def stop_timer(self):
        self.timer_running = False
        self.elapsed_time = 0
        if self.timer_value is not None:
            self.timer_value.set("00:00:00")
        self.reset_distance()
        self.finish_csv_logging()

    def update_timer(self):
        if self.timer_running:
            elapsed = int(time.time() - self.start_time)
            hours, rem = divmod(elapsed, 3600)
            minutes, seconds = divmod(rem, 60)
            time_str = f"{hours:02d}:{minutes:02d}:{seconds:02d}"
            if self.timer_value is not None:
                self.timer_value.set(time_str)
            threading.Timer(1, self.update_timer).start()

    def reset_distance(self):
        self.total_distance = 0
        if self.distance_value is not None:
            self.distance_value.set("0.00")
        self.last_update_time = None

    def start_csv_logging(self):
        self.start_datetime = datetime.now()
        directory = "workouts"
        if not os.path.exists(directory):
            os.makedirs(directory)
        filename = os.path.join(directory, f"workout_{self.start_datetime.strftime('%Y%m%d_%H%M%S')}.csv")
        self.csv_file = open(filename, 'w', newline='')
        self.csv_writer = csv.DictWriter(self.csv_file, fieldnames=['timestamp', 'heart_rate', 'power', 'cadence', 'speed', 'distance'])
        self.csv_writer.writeheader()

    def finish_csv_logging(self):
        if self.csv_file:
            self.csv_file.close()
            print(f"CSV file saved: {self.csv_file.name}")
            self.csv_file = None
            self.csv_writer = None

def create_custom_title_bar(window, title):
    title_label = tk.Label(window, text=title, bg="#2B2B2B", fg="white") # maybe set  bg="systemTransparent" in future...
    title_label.pack(fill='both')

    def start_move(event):
        window.x = event.x
        window.y = event.y

    def stop_move(event):
        window.x = None
        window.y = None

    def do_move(event):
        x = window.winfo_pointerx() - window.x
        y = window.winfo_pointery() - window.y
        window.geometry(f"+{x}+{y}")

    title_label.bind("<ButtonPress-1>", start_move)
    title_label.bind("<ButtonRelease-1>", stop_move)
    title_label.bind("<B1-Motion>", do_move)

def create_value_window(title, x_offset, y_offset, handler_value, layout=None):
    window = tk.Toplevel()
    window.overrideredirect(True)
    create_custom_title_bar(window, title)
    
    if layout and title in layout:
        window.geometry(layout[title]["geometry"])
    else:
        window.geometry(f"300x200+{x_offset}+{y_offset}")
    
    window.update_idletasks()
    window.wm_attributes("-topmost", True)
    window.resizable(True, True)
    window.wm_attributes('-transparent', True)

    value = tk.IntVar(window, value=0)
    setattr(handler, handler_value, value)

    label = tk.Label(window, textvariable=value, bg="systemTransparent", font=("Arial", 20), bd=0)
    label.pack(expand=True, fill="both")

    def adjust_text_size(event=None):
        new_width = window.winfo_width()
        new_height = window.winfo_height()
        font_percentage = 0.7
        new_font_size = int(min(new_width, new_height) * font_percentage)
        # if layout and title in layout:
        #     new_font_size = int(layout[title]["font_size"])
        label.config(font=("Helvetica", new_font_size, "bold"), padx=0, pady=0)

    window.bind("<Configure>", adjust_text_size)
    adjust_text_size()
    
    return window


def round_rectangle(canvas, x1, y1, x2, y2, radius=25, **kwargs):
    points = [x1+radius, y1,
              x1+radius, y1,
              x2-radius, y1,
              x2-radius, y1,
              x2, y1,
              x2, y1+radius,
              x2, y1+radius,
              x2, y2-radius,
              x2, y2-radius,
              x2, y2,
              x2-radius, y2,
              x2-radius, y2,
              x1+radius, y2,
              x1+radius, y2,
              x1, y2,
              x1, y2-radius,
              x1, y2-radius,
              x1, y1+radius,
              x1, y1+radius,
              x1, y1]
    return canvas.create_polygon(points, **kwargs, smooth=True)
class SimpleButton:
    def __init__(self, canvas, side, command=None, textvariable=None,**kwargs):
        self.canvas = canvas
        self.command = command
        self.side = side
        self.textvariable =textvariable
        self.canvas_width = canvas.winfo_width()
        
        if self.side == "left":
            x1, x2 = 0, self.canvas_width // 4
        elif self.side == "right":
            x1, x2 = self.canvas_width -  self.canvas_width // 4, self.canvas_width
        else:
            raise ValueError("Side must be 'left' or 'right'")
        
        self.rect_id = canvas.create_rectangle(x1, 0, x2, 0, fill="white")
        self.text_id = canvas.create_text(50, 25, text=self.textvariable, fill="black", font=("Arial", 12, "normal"))
        
        self.canvas.tag_bind(self.rect_id, "<Button-1>", self.on_click)
        self.canvas.tag_bind(self.text_id, "<Button-1>", self.on_click)

    def on_click(self, event):
        if self.command:
            print("Button clicked")
            print(self.command)
            self.command()

    def update_button_size(self, canvas_width, new_height):
        """Update the size and position of the button based on new dimensions."""
        if self.rect_id:
            self.canvas.delete(self.rect_id)
        if self.text_id:
            self.canvas.delete(self.text_id)
        
        if self.side == "left":
            x1, x2 = 0, canvas_width // 4
        elif self.side == "right":
            x1, x2 = canvas_width -  canvas_width // 4, canvas_width
        else:
            raise ValueError("Side must be 'left' or 'right'")
        
        y1, y2 = 0, new_height
        self.rect_id = round_rectangle(self.canvas, x1, y1, x2, y2, radius=18, fill="white")
        center_x = (x1 + x2) // 2
        center_y = (y1 + y2) // 2
        self.text_id = self.canvas.create_text(center_x, center_y, text=self.textvariable.get(), fill="black", font=("Arial", 12, "normal"))
        # if self.textvariable:
        #     self.textvariable.trace("w", self.update_text)

    def update_text(self, *args):
        """Updates the text displayed on the button when the StringVar changes."""
        self.canvas.itemconfig(self.text_id, text=self.textvariable.get())

class CustomButton:
    def __init__(self, canvas, side, radius=25, textvariable=None, command=None, **kwargs):
        self.canvas = canvas
        self.command = command
        self.side = side
        self.textvariable = textvariable
        canvas_width=100
        # Default coordinates; these will be updated later
        self.rect_id = None
        self.text_id = None
        
        if self.rect_id:
            self.canvas.delete(self.rect_id)
        if self.text_id:
            self.canvas.delete(self.text_id)
        
        if self.side == "left":
            x1, x2 = 0, canvas_width // 4
        elif self.side == "right":
            x1, x2 = canvas_width -  canvas_width // 4, canvas_width
        else:
            raise ValueError("Side must be 'left' or 'right'")
        
        y1=y2 = 0
        self.rect_id = canvas.create_rectangle( x1, y1, x2, y2,  fill="white")
        center_x = (x1 + x2) // 2
        center_y = (y1 + y2) // 2
        self.text_id = self.canvas.create_text(center_x, center_y, text=self.textvariable.get(), fill="black", font=("Arial", 12, "normal"))
        # if self.textvariable:
        #     self.textvariable.trace("w", self.update_text)


       
        self.canvas.tag_bind(self.rect_id, "<Button-1>", self.on_click)
        self.canvas.tag_bind(self.text_id, "<Button-1>", self.on_click)
    
    def update_text(self, *args):
        """Updates the text displayed on the button when the StringVar changes."""
        self.canvas.itemconfig(self.text_id, text=self.textvariable.get())

    def on_click(self, event):
        print("tepre")
        if self.command:
            print("tepre")
            self.command()

    def update_button_size(self, canvas_width, new_height):
        """Update the size and position of the button based on new dimensions."""
        if self.rect_id:
            self.canvas.delete(self.rect_id)
        if self.text_id:
            self.canvas.delete(self.text_id)
        
        if self.side == "left":
            x1, x2 = 0, canvas_width // 4
        elif self.side == "right":
            x1, x2 = canvas_width -  canvas_width // 4, canvas_width
        else:
            raise ValueError("Side must be 'left' or 'right'")
        
        y1, y2 = 0, new_height
        self.rect_id = round_rectangle(self.canvas, x1, y1, x2, y2, radius=18, fill="white")
        center_x = (x1 + x2) // 2
        center_y = (y1 + y2) // 2
        self.text_id = self.canvas.create_text(center_x, center_y, text=self.textvariable.get(), fill="black", font=("Arial", 12, "normal"))
        if self.textvariable:
            self.textvariable.trace("w", self.update_text)


def create_timer_window(handler, x_offset, y_offset, layout=None):
    def adjust_text_size(event=None):
        new_width = window.winfo_width()
        new_height = window.winfo_height()
        font_percentage = 0.5
        new_font_size = int(min(new_width, new_height) * font_percentage)
        timer_label.config(font=("Helvetica", new_font_size, "bold"), padx=0, pady=0)

    def stop_timer_and_reset_button():
        handler.stop_timer()
        start_btn_text.set("Start")

    def toggle_start_pause(event=None):
        if handler.timer_running:
            handler.pause_timer()
            start_btn_text.set("Resume")
        else:
            handler.start_timer()
            start_btn_text.set("Pause")

    # def update_canvas(event):
        # window_width = window.winfo_width()
        # window_height = window.winfo_height()
        
        # # Update button frame size
        # button_frame.config(width=window_width)
        # button_frame.config(height=int(0.2 * window_height))
        
        # # Re-position buttons
        # start_pause_button.canvas.coords(start_pause_button.rect_id, 10, 10, (window_width // 2) - 5, 40)
        # start_pause_button.canvas.coords(start_pause_button.text_id, (10 + (window_width // 2) - 5) // 2, 25)
        # stop_button.canvas.coords(stop_button.rect_id, (window_width // 2) + 5, 10, window_width - 10, 40)
        # stop_button.canvas.coords(stop_button.text_id, ((window_width // 2) + 5 + window_width - 10) // 2, 25)
    def update_canvas(event):
        window_width = window.winfo_width()
        window_height = window.winfo_height()
        
        # Define min and max heights for the button frame
        min_height = 20
        max_height = 50
        
        # Calculate new height, ensuring it is within the min and max bounds
        new_height = int(0.1 * window_height)
        new_height = max(min_height, min(new_height, max_height))

        # Update button frame size
        button_frame.config(width=window_width, height=new_height)
        
        # Update button size to fit the new height
        # start_pause_button.update_button_size(window_width, new_height)
        # stop_button.update_button_size(window_width, new_height)



    window = tk.Toplevel()
    window.overrideredirect(True)
    create_custom_title_bar(window, "Workout Timer")

    if layout and "Timer" in layout:
        window.geometry(layout["Timer"]["geometry"])
    else:
        window.geometry(f"300x250+{x_offset}+{y_offset}")
    
    window.update_idletasks()
    window.wm_attributes("-topmost", True)
    window.resizable(True, True)
    window.wm_attributes('-transparent', True)

    timer_value = tk.StringVar(window, value="00:00:00")
    handler.timer_value = timer_value

    timer_label = tk.Label(window, textvariable=timer_value, bg="systemTransparent", font=("Arial", 12), bd=0)
    timer_label.pack(expand=True, fill="both")

    button_frame = tk.Canvas(window, bg="systemTransparent", bd=0, highlightthickness=0)
    button_frame.pack(fill="x", padx=0, pady=0)

    # # Create custom Start/Pause button
    start_btn_text = tk.StringVar(window, value="Start")
    # start_pause_button = CustomButton(button_frame, textvariable=start_btn_text,
                                    #   fill="white", command=toggle_start_pause, side = "left")
 

    # Create custom Stop button
    # stop_button = CustomButton(button_frame, textvariable=tk.StringVar(value="Stop"),
                            #    fill="white", command=stop_timer_and_reset_button, side="right")
 
    # start_btn_text = tk.StringVar(window, value="Start")
    start_pause_button = SimpleButton(button_frame, side="left", textvariable=start_btn_text,
                                      command=toggle_start_pause)
 
    # # Create custom Stop button
    stop_button = SimpleButton(button_frame, side="right", textvariable=tk.StringVar(value="Stop"),
                               command=stop_timer_and_reset_button)

    # testbtn = SimpleButton(button_frame,side="right",command=toggle_start_pause)                       

    window.bind("<Configure>", adjust_text_size)
    window.bind("<Configure>", update_canvas)
    adjust_text_size()

    return window

async def run_bluetooth(ftms_address, hr_address, handler):
    async with BleakClient(ftms_address, timeout=5) as ftms_client, BleakClient(hr_address, timeout=10) as hr_client:
        ftms = FitnessMachineService(ftms_client)
        hr_service = HeartRateService(hr_client)

        def print_indoor_bike_data(data):
            handler.update_indoor_bike_data(data)

        def print_heart_rate_data(data):
            handler.update_heart_rate(data)

        ftms.set_indoor_bike_data_handler(print_indoor_bike_data)
        await ftms.enable_indoor_bike_data_notify()

        await ftms.enable_control_point_indicate()
        await ftms.request_control()
        await ftms.reset()

        hr_service.set_hr_measurement_handler(print_heart_rate_data)
        await hr_service.enable_hr_measurement_notifications()

        while True:
            await asyncio.sleep(1)

def start_bluetooth(ftms_address, hr_address, handler):
    def bluetooth_thread_function():
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        print(f"Attempting connection to devices... ")
        while True:
            try:
                loop.run_until_complete(run_bluetooth(ftms_address, hr_address, handler))
            except Exception as e:
                print(f"Bluetooth Error, retrying... {e}")
                continue
    # def bluetooth_thread_function():
    #     loop = asyncio.new_event_loop()
    #     asyncio.set_event_loop(loop)
    #     try:
    #         loop.run_until_complete(run_bluetooth(ftms_address, hr_address, handler))
    #     except Exception as e:
    #         print(f"Bluetooth Error: {e}")
    #     finally:
    #         loop.run_until_complete(loop.shutdown_asyncgens())
    #         loop.close()

    bluetooth_thread = threading.Thread(target=bluetooth_thread_function, daemon=True)
    bluetooth_thread.start()

# Main window setup
main_window = tk.Tk()
main_window.withdraw()

# Create menu bar
menu_bar = tk.Menu(main_window)
main_window.config(menu=menu_bar)

# Create File menu
file_menu = tk.Menu(menu_bar, tearoff=0)
menu_bar.add_cascade(label="File", menu=file_menu)

# Create an instance of the data handler
handler = BluetoothDataHandler()

# Load saved layout
saved_layout = load_layout()

# Create the windows
windows = {
    "Heart Rate": create_value_window("Heart Rate", 100, 0, "heart_rate_value", saved_layout),
    "Power": create_value_window("Power", 600, 0, "power_value", saved_layout),
    "Cadence": create_value_window("Cadence", 100, 400, "cadence_value", saved_layout),
    "Speed": create_value_window("Speed", 600, 400, "speed_value", saved_layout),
    "Distance": create_value_window("Distance", 350, 600, "distance_value", saved_layout),
    "Timer": create_timer_window(handler, 350, 200, saved_layout)
}

# Add Save Layout option to File menu
file_menu.add_command(label="Save Layout", command=lambda: save_layout(windows))

# Start the Bluetooth process
suito_adr = "0E0DB68C-0E0A-2490-11A1-2A4F4925151C"
heart_rate_adr = "F1982529-2BF7-810D-ED72-372360D681F3"  
start_bluetooth(suito_adr, heart_rate_adr, handler)

# Save layout on exit
def on_closing():
    save_layout(windows)
    print("saved layout")
    main_window.destroy()

main_window.protocol("WM_DELETE_WINDOW", on_closing)

# Start the Tkinter event loop
main_window.mainloop()