import sys
import logging
import asyncio
import threading
import struct
import random

from typing import Any, Union

from bless import (
    BlessServer,
    BlessGATTCharacteristic,
    GATTCharacteristicProperties,
    GATTAttributePermissions,
)

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(name=__name__)

# Power Meter Service and Characteristic UUIDs
POWER_SERVICE_UUID = "00001818-0000-1000-8000-00805f9b34fb"
POWER_CHARACTERISTIC_UUID = "00002a63-0000-1000-8000-00805f9b34fb"

trigger: Union[asyncio.Event, threading.Event]
if sys.platform in ["darwin", "win32"]:
    trigger = threading.Event()
else:
    trigger = asyncio.Event()

class FakePowerMeter:
    def __init__(self):
        self.power = 150  # Initial power in watts
        self.cadence = 80  # Initial cadence in RPM

    def generate_power_data(self):
        # Simulate some variation in power and cadence
        self.power += random.randint(-10, 10)
        self.power = max(0, min(400, self.power))  # Keep power between 0 and 400 watts

        self.cadence += random.randint(-5, 5)
        self.cadence = max(0, min(120, self.cadence))  # Keep cadence between 0 and 120 RPM

        # Pack data according to Cycling Power Measurement characteristic format
        flags = 0x20  # Bit 5 set: Pedal Power Balance Present
        power = self.power
        balance = 50  # 50% left/right balance

        return struct.pack("<HHHH", flags, power, 0, (balance << 8) | self.cadence)

power_meter = FakePowerMeter()

def read_request(characteristic: BlessGATTCharacteristic, **kwargs) -> bytearray:
    logger.debug(f"Reading {characteristic.value}")
    return characteristic.value

def write_request(characteristic: BlessGATTCharacteristic, value: Any, **kwargs):
    characteristic.value = value
    logger.debug(f"Char value set to {characteristic.value}")
    if characteristic.value == b"\x0f":
        logger.debug("Power meter stopped")
        trigger.set()

async def update_power_data(server):
    while not trigger.is_set():
        new_data = power_meter.generate_power_data()
        server.get_characteristic(POWER_CHARACTERISTIC_UUID).value = new_data
        server.update_value(POWER_SERVICE_UUID, POWER_CHARACTERISTIC_UUID)
        await asyncio.sleep(1)

async def run(loop):
    trigger.clear()
    
    # Instantiate the server
    server = BlessServer(name="Fake Power Meter", loop=loop)
    server.read_request_func = read_request
    server.write_request_func = write_request

    # Add Power Meter Service
    await server.add_new_service(POWER_SERVICE_UUID)

    # Add Power Measurement Characteristic
    char_flags = GATTCharacteristicProperties.read | GATTCharacteristicProperties.notify
    permissions = GATTAttributePermissions.readable
    await server.add_new_characteristic(
        POWER_SERVICE_UUID, POWER_CHARACTERISTIC_UUID, char_flags, None, permissions
    )

    await server.start()
    logger.info("Fake Power Meter is advertising")
    logger.info(f"Power Measurement Characteristic UUID: {POWER_CHARACTERISTIC_UUID}")

    # Set initial value after starting the server
    initial_data = power_meter.generate_power_data()
    server.get_characteristic(POWER_CHARACTERISTIC_UUID).value = initial_data
    server.update_value(POWER_SERVICE_UUID, POWER_CHARACTERISTIC_UUID)

    # Start updating power data
    update_task = asyncio.create_task(update_power_data(server))

    if trigger.__module__ == "threading":
        trigger.wait()
    else:
        await trigger.wait()

    update_task.cancel()
    await server.stop()

if __name__ == "__main__":
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(run(loop))