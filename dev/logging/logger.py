import asyncio
import tkinter as tk
from bleak import BleakClient
from pycycling.fitness_machine_service import FitnessMachineService
from pycycling.heart_rate_service import HeartRateService
import threading
import time
from fitparse import FitFile
from fitparse import DataMessage
from fitparse import records_as_string
from datetime import datetime, timedelta
import os

class BluetoothDataHandler:
    def __init__(self):
        self.heart_rate_value = None
        self.power_value = None
        self.cadence_value = None
        self.speed_value = None
        self.distance_value = None
        self.timer_value = None
        self.timer_running = False
        self.start_time = 0
        self.elapsed_time = 0
        self.workout_data = []
        self.total_distance = 0

    def update_indoor_bike_data(self, data):
        if self.power_value is not None:
            self.power_value.set(data.instant_power)
        if self.cadence_value is not None:
            self.cadence_value.set(data.instant_cadence)
        if self.speed_value is not None:
            self.speed_value.set(data.instant_speed)
        
        # Calculate and update distance
        if self.timer_running:
            time_diff = time.time() - self.start_time - self.elapsed_time
            distance_diff = (data.instant_speed / 3.6) * time_diff  # Convert km/h to m/s
            self.total_distance += distance_diff
            if self.distance_value is not None:
                self.distance_value.set(f"{self.total_distance/1000:.2f}")  # Convert m to km

        # Log workout data
        if self.timer_running:
            self.workout_data.append({
                'timestamp': datetime.now(),
                'heart_rate': self.heart_rate_value.get() if self.heart_rate_value else None,
                'power': data.instant_power,
                'cadence': data.instant_cadence,
                'speed': data.instant_speed,
                'distance': self.total_distance
            })

    def update_heart_rate(self, data):
        if self.heart_rate_value is not None:
            self.heart_rate_value.set(data.heart_rate)

    def start_timer(self):
        if not self.timer_running:
            self.timer_running = True
            self.start_time = time.time() - self.elapsed_time
            self.update_timer()

    def pause_timer(self):
        if self.timer_running:
            self.timer_running = False
            self.elapsed_time = time.time() - self.start_time

    def stop_timer(self):
        self.timer_running = False
        self.elapsed_time = 0
        if self.timer_value is not None:
            self.timer_value.set("00:00:00")
        self.create_fit_file()
        self.total_distance = 0
        if self.distance_value is not None:
            self.distance_value.set("0.00")
        self.workout_data = []

    def update_timer(self):
        if self.timer_running:
            elapsed = int(time.time() - self.start_time)
            hours, rem = divmod(elapsed, 3600)
            minutes, seconds = divmod(rem, 60)
            time_str = f"{hours:02d}:{minutes:02d}:{seconds:02d}"
            if self.timer_value is not None:
                self.timer_value.set(time_str)
            threading.Timer(1, self.update_timer).start()

    def create_fit_file(self):
        if not self.workout_data:
            print("No workout data to save.")
            return

        fit_file = FitFile()
        start_time = self.workout_data[0]['timestamp']

        # Create file_id message
        file_id = DataMessage('file_id')
        file_id.set_field('type', 'activity')
        file_id.set_field('manufacturer', 'development')
        file_id.set_field('product', 1)
        file_id.set_field('time_created', start_time)
        fit_file.add_message(file_id)

        # Create record messages
        for data in self.workout_data:
            record = DataMessage('record')
            record.set_field('timestamp', data['timestamp'])
            record.set_field('heart_rate', data['heart_rate'])
            record.set_field('power', data['power'])
            record.set_field('cadence', data['cadence'])
            record.set_field('speed', data['speed'])
            record.set_field('distance', data['distance'])
            fit_file.add_message(record)

        # Create session message
        session = DataMessage('session')
        session.set_field('start_time', start_time)
        session.set_field('total_elapsed_time', (self.workout_data[-1]['timestamp'] - start_time).total_seconds())
        session.set_field('total_distance', self.workout_data[-1]['distance'])
        session.set_field('avg_power', sum(d['power'] for d in self.workout_data) / len(self.workout_data))
        session.set_field('max_power', max(d['power'] for d in self.workout_data))
        session.set_field('avg_cadence', sum(d['cadence'] for d in self.workout_data) / len(self.workout_data))
        session.set_field('max_cadence', max(d['cadence'] for d in self.workout_data))
        session.set_field('avg_heart_rate', sum(d['heart_rate'] for d in self.workout_data if d['heart_rate']) / len(self.workout_data))
        session.set_field('max_heart_rate', max(d['heart_rate'] for d in self.workout_data if d['heart_rate']))
        fit_file.add_message(session)

        # Save the FIT file
        filename = f"workout_{start_time.strftime('%Y%m%d_%H%M%S')}.fit"
        with open(filename, 'wb') as f:
            fit_file.to_bytes(f)

        print(f"Workout data saved to {filename}")

def create_custom_title_bar(window, title):
    title_label = tk.Label(window, text=title, bg="#2B2B2B", fg="white")
    title_label.pack(fill='both')

    def start_move(event):
        window.x = event.x
        window.y = event.y

    def stop_move(event):
        window.x = None
        window.y = None

    def do_move(event):
        x = window.winfo_pointerx() - window.x
        y = window.winfo_pointery() - window.y
        window.geometry(f"+{x}+{y}")

    title_label.bind("<ButtonPress-1>", start_move)
    title_label.bind("<ButtonRelease-1>", stop_move)
    title_label.bind("<B1-Motion>", do_move)

def create_value_window(title, x_offset, y_offset, handler_value):
    window = tk.Toplevel()
    window.overrideredirect(True)
    create_custom_title_bar(window, title)
    window.geometry(f"300x200+{x_offset}+{y_offset}")
    window.update_idletasks()
    window.wm_attributes("-topmost", True)
    window.resizable(True, True)
    window.wm_attributes('-transparent', True)

    value = tk.StringVar(window, value="0")
    setattr(handler, handler_value, value)

    label = tk.Label(window, textvariable=value, bg="systemTransparent", font=("Arial", 20), bd=0)
    label.pack(expand=True, fill="both")

    def adjust_text_size(event=None):
        new_width = window.winfo_width()
        new_height = window.winfo_height()
        font_percentage = 0.7
        new_font_size = int(min(new_width, new_height) * font_percentage)
        label.config(font=("Helvetica", new_font_size, "bold"), padx=0, pady=0)

    window.bind("<Configure>", adjust_text_size)
    adjust_text_size()

def create_timer_window(handler, x_offset, y_offset):
    window = tk.Toplevel()
    window.overrideredirect(True)
    create_custom_title_bar(window, "Workout Timer")
    window.geometry(f"300x250+{x_offset}+{y_offset}")
    window.update_idletasks()
    window.wm_attributes("-topmost", True)
    window.resizable(True, True)
    window.wm_attributes('-transparent', True)

    timer_value = tk.StringVar(window, value="00:00:00")
    handler.timer_value = timer_value

    timer_label = tk.Label(window, textvariable=timer_value, bg="systemTransparent", font=("Arial", 20), bd=0)
    timer_label.pack(expand=True, fill="both")

    button_frame = tk.Frame(window, bg="systemTransparent")
    button_frame.pack(fill="x", padx=10, pady=10)

    start_pause_button = tk.Button(button_frame, text="Start", command=lambda: toggle_start_pause(start_pause_button))
    start_pause_button.pack(side="left", padx=5)

    stop_button = tk.Button(button_frame, text="Stop", command=handler.stop_timer)
    stop_button.pack(side="right", padx=5)

    def toggle_start_pause(button):
        if handler.timer_running:
            handler.pause_timer()
            button.config(text="Start")
        else:
            handler.start_timer()
            button.config(text="Pause")

    def adjust_text_size(event=None):
        new_width = window.winfo_width()
        new_height = window.winfo_height()
        font_percentage = 0.5
        new_font_size = int(min(new_width, new_height) * font_percentage)
        timer_label.config(font=("Helvetica", new_font_size, "bold"), padx=0, pady=0)

    window.bind("<Configure>", adjust_text_size)
    adjust_text_size()

async def run_bluetooth(ftms_address, hr_address, handler):
    async with BleakClient(ftms_address, timeout=30) as ftms_client, BleakClient(hr_address, timeout=30) as hr_client:
        ftms = FitnessMachineService(ftms_client)
        hr_service = HeartRateService(hr_client)

        def print_indoor_bike_data(data):
            handler.update_indoor_bike_data(data)

        def print_heart_rate_data(data):
            handler.update_heart_rate(data)

        ftms.set_indoor_bike_data_handler(print_indoor_bike_data)
        await ftms.enable_indoor_bike_data_notify()

        await ftms.enable_control_point_indicate()
        await ftms.request_control()
        await ftms.reset()

        hr_service.set_hr_measurement_handler(print_heart_rate_data)
        await hr_service.enable_hr_measurement_notifications()

        while True:
            await asyncio.sleep(1)

def start_bluetooth(ftms_address, hr_address, handler):
    def bluetooth_thread_function():
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        while True:
            try:
                loop.run_until_complete(run_bluetooth(ftms_address, hr_address, handler))
            except Exception as e:
                print(f"Bluetooth Error, retrying... {e}")
                continue

    bluetooth_thread = threading.Thread(target=bluetooth_thread_function, daemon=True)
    bluetooth_thread.start()

# Main window setup
main_window = tk.Tk()
main_window.withdraw()

# Create an instance of the data handler
handler = BluetoothDataHandler()

# Create the windows
create_value_window("Heart Rate", 100, 0, "heart_rate_value")
create_value_window("Power", 600, 0, "power_value")
create_value_window("Cadence", 100, 400, "cadence_value")
create_value_window("Speed", 600, 400, "speed_value")
create_value_window("Distance (km)", 350, 400, "distance_value")
create_timer_window(handler, 350, 200)

# Start the Bluetooth process
suito_adr = "0E0DB68C-0E0A-2490-11A1-2A4F4925151C"
heart_rate_adr = "F1982529-2BF7-810D-ED72-372360D681F3"  
start_bluetooth(suito_adr, heart_rate_adr, handler)

# Start the Tkinter event loop
main_window.mainloop()