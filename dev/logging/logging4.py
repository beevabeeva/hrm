import asyncio
import tkinter as tk
from bleak import BleakClient
from pycycling.fitness_machine_service import FitnessMachineService
from pycycling.heart_rate_service import HeartRateService
import threading
import os
import json
import time
import csv
from datetime import datetime, timedelta

LAYOUT_FILE = "window_layout.json"

def save_layout(windows):
    layout = {}
    for title, window in windows.items():
        geometry = window.geometry()
        font_size = window.winfo_children()[1].cget("font").split()[1]  # Get font size from label
        layout[title] = {"geometry": geometry, "font_size": font_size}
    
    with open(LAYOUT_FILE, "w") as f:
        json.dump(layout, f)
    print("Layout saved successfully!")


def load_layout():
    if os.path.exists(LAYOUT_FILE):
        with open(LAYOUT_FILE, "r") as f:
            return json.load(f)
    return None


class BluetoothDataHandler:
    def __init__(self):
        self.heart_rate_value = None
        self.power_value = None
        self.cadence_value = None
        self.speed_value = None
        self.distance_value = None
        self.timer_value = None
        self.timer_running = False
        self.start_time = 0
        self.elapsed_time = 0
        self.total_distance = 0
        self.csv_file = None
        self.csv_writer = None
        self.start_datetime = None
        

    def update_indoor_bike_data(self, data):
        if self.power_value is not None:
            self.power_value.set(data.instant_power)
        if self.cadence_value is not None:
            self.cadence_value.set(data.instant_cadence)
        if self.speed_value is not None:
            self.speed_value.set(data.instant_speed)
        
        # Update distance
        if self.distance_value is not None:
            self.total_distance += data.instant_speed * 1000 / 3600  # Convert km/h to m/s
            self.distance_value.set(f"{self.total_distance / 1000:.2f}")  # Display in km
            print("total_distance")
            print(self.total_distance)
            print("distance_value")
            print(self.distance_value)
        # Log data to CSV file
        if self.csv_writer and self.timer_running:
            self.csv_writer.writerow({
                'timestamp': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                'heart_rate': self.heart_rate_value.get() if self.heart_rate_value else '',
                'power': self.power_value.get() if self.power_value else '',
                'cadence': self.cadence_value.get() if self.cadence_value else '',
                'speed': self.speed_value.get() if self.speed_value else '',
                'distance': self.total_distance
            })

    def update_heart_rate(self, data):
        if self.heart_rate_value is not None:
            self.heart_rate_value.set(data.heart_rate)

    def start_timer(self):
        if not self.timer_running:
            self.timer_running = True
            self.start_time = time.time() - self.elapsed_time
            self.update_timer()
            if not self.csv_file:
                self.start_csv_logging()

    def pause_timer(self):
        if self.timer_running:
            self.timer_running = False
            self.elapsed_time = time.time() - self.start_time

    def stop_timer(self):
        self.timer_running = False
        self.elapsed_time = 0
        if self.timer_value is not None:
            self.timer_value.set("00:00:00")
        self.finish_csv_logging()

    def update_timer(self):
        if self.timer_running:
            elapsed = int(time.time() - self.start_time)
            hours, rem = divmod(elapsed, 3600)
            minutes, seconds = divmod(rem, 60)
            time_str = f"{hours:02d}:{minutes:02d}:{seconds:02d}"
            if self.timer_value is not None:
                self.timer_value.set(time_str)
            threading.Timer(1, self.update_timer).start()

    def start_csv_logging(self):
        self.start_datetime = datetime.now()
        filename = f"workout_{self.start_datetime.strftime('%Y%m%d_%H%M%S')}.csv"
        self.csv_file = open(filename, 'w', newline='')
        self.csv_writer = csv.DictWriter(self.csv_file, fieldnames=['timestamp', 'heart_rate', 'power', 'cadence', 'speed', 'distance'])
        self.csv_writer.writeheader()

    def finish_csv_logging(self):
        if self.csv_file:
            self.csv_file.close()
            print(f"CSV file saved: {self.csv_file.name}")
            self.csv_file = None
            self.csv_writer = None

def create_custom_title_bar(window, title):
    title_label = tk.Label(window, text=title, bg="#2B2B2B", fg="white")
    title_label.pack(fill='both')

    def start_move(event):
        window.x = event.x
        window.y = event.y

    def stop_move(event):
        window.x = None
        window.y = None

    def do_move(event):
        x = window.winfo_pointerx() - window.x
        y = window.winfo_pointery() - window.y
        window.geometry(f"+{x}+{y}")

    title_label.bind("<ButtonPress-1>", start_move)
    title_label.bind("<ButtonRelease-1>", stop_move)
    title_label.bind("<B1-Motion>", do_move)

def create_value_window(title, x_offset, y_offset, handler_value, layout=None):
    window = tk.Toplevel()
    window.overrideredirect(True)
    create_custom_title_bar(window, title)
    
    if layout and title in layout:
        window.geometry(layout[title]["geometry"])
    else:
        window.geometry(f"300x200+{x_offset}+{y_offset}")
    
    window.update_idletasks()
    window.wm_attributes("-topmost", True)
    window.resizable(True, True)
    window.wm_attributes('-transparent', True)

    value = tk.IntVar(window, value=0)
    setattr(handler, handler_value, value)

    label = tk.Label(window, textvariable=value, bg="systemTransparent", font=("Arial", 20), bd=0)
    label.pack(expand=True, fill="both")

    def adjust_text_size(event=None):
        new_width = window.winfo_width()
        new_height = window.winfo_height()
        font_percentage = 0.7
        new_font_size = int(min(new_width, new_height) * font_percentage)
        if layout and title in layout:
            new_font_size = int(layout[title]["font_size"])
        label.config(font=("Helvetica", new_font_size, "bold"), padx=0, pady=0)

    window.bind("<Configure>", adjust_text_size)
    adjust_text_size()
    
    return window

def create_timer_window(handler, x_offset, y_offset,layout=None):
    window = tk.Toplevel()
    window.overrideredirect(True)
    create_custom_title_bar(window, "Workout Timer")

    if layout and "Timer" in layout:
        window.geometry(layout["Timer"]["geometry"])
    else:
        window.geometry(f"300x250+{x_offset}+{y_offset}")
    
    window.update_idletasks()
    window.wm_attributes("-topmost", True)
    window.resizable(True, True)
    window.wm_attributes('-transparent', True)

    timer_value = tk.StringVar(window, value="00:00:00")
    handler.timer_value = timer_value

    timer_label = tk.Label(window, textvariable=timer_value, bg="systemTransparent", font=("Arial", 12), bd=0)
    timer_label.pack(expand=True, fill="both")

    button_frame = tk.Frame(window, bg="systemTransparent")
    button_frame.pack(fill="x", padx=10, pady=10)

    start_pause_button = tk.Button(button_frame, text="Start", command=lambda: toggle_start_pause(start_pause_button))
    start_pause_button.pack(side="left", padx=5)

    stop_button = tk.Button(button_frame, text="Stop", command=handler.stop_timer)
    stop_button.pack(side="right", padx=5)

    def toggle_start_pause(button):
        if handler.timer_running:
            handler.pause_timer()
            button.config(text="Start")
        else:
            handler.start_timer()
            button.config(text="Pause")

    def adjust_text_size(event=None):
        new_width = window.winfo_width()
        new_height = window.winfo_height()
        font_percentage = 0.5
        new_font_size = int(min(new_width, new_height) * font_percentage)
        if layout and "Timer" in layout:
            new_font_size = int(layout["Timer"]["font_size"])
        timer_label.config(font=("Helvetica", new_font_size, "bold"), padx=0, pady=0)

    window.bind("<Configure>", adjust_text_size)
    adjust_text_size()

    return window

async def run_bluetooth(ftms_address, hr_address, handler):
    async with BleakClient(ftms_address, timeout=30) as ftms_client, BleakClient(hr_address, timeout=30) as hr_client:
        ftms = FitnessMachineService(ftms_client)
        hr_service = HeartRateService(hr_client)

        def print_indoor_bike_data(data):
            handler.update_indoor_bike_data(data)

        def print_heart_rate_data(data):
            handler.update_heart_rate(data)

        ftms.set_indoor_bike_data_handler(print_indoor_bike_data)
        await ftms.enable_indoor_bike_data_notify()

        await ftms.enable_control_point_indicate()
        await ftms.request_control()
        await ftms.reset()

        hr_service.set_hr_measurement_handler(print_heart_rate_data)
        await hr_service.enable_hr_measurement_notifications()

        while True:
            await asyncio.sleep(1)

def start_bluetooth(ftms_address, hr_address, handler):
    def bluetooth_thread_function():
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        while True:
            try:
                loop.run_until_complete(run_bluetooth(ftms_address, hr_address, handler))
            except Exception as e:
                print(f"Bluetooth Error, retrying... {e}")
                continue

    bluetooth_thread = threading.Thread(target=bluetooth_thread_function, daemon=True)
    bluetooth_thread.start()

# Main window setup
main_window = tk.Tk()
main_window.withdraw()

# Create menu bar
menu_bar = tk.Menu(main_window)
main_window.config(menu=menu_bar)

# Create File menu
file_menu = tk.Menu(menu_bar, tearoff=0)
menu_bar.add_cascade(label="File", menu=file_menu)

# Create an instance of the data handler
handler = BluetoothDataHandler()

# Load saved layout
saved_layout = load_layout()

# Create the windows
windows = {
    "Heart Rate": create_value_window("Heart Rate", 100, 0, "heart_rate_value", saved_layout),
    "Power": create_value_window("Power", 600, 0, "power_value", saved_layout),
    "Cadence": create_value_window("Cadence", 100, 400, "cadence_value", saved_layout),
    "Speed": create_value_window("Speed", 600, 400, "speed_value", saved_layout),
    "Timer": create_timer_window(handler, 350, 200, saved_layout)
}

# Add Save Layout option to File menu
file_menu.add_command(label="Save Layout", command=lambda: save_layout(windows))

# Start the Bluetooth process
suito_adr = "0E0DB68C-0E0A-2490-11A1-2A4F4925151C"
heart_rate_adr = "F1982529-2BF7-810D-ED72-372360D681F3"  
start_bluetooth(suito_adr, heart_rate_adr, handler)

# Save layout on exit
def on_closing():
    save_layout(windows)
    print("saved layout")
    main_window.destroy()

main_window.protocol("WM_DELETE_WINDOW", on_closing)

# Start the Tkinter event loop
main_window.mainloop()