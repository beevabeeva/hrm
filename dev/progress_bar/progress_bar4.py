from tkinter import * 
from tkinter.ttk import *
import customtkinter
# creating tkinter window 
root = Tk() 
root.geometry("300x200")

# Progress bar widget 
# progress = Progressbar(root, orient = HORIZONTAL, 
#               length = 100, mode = 'determinate') 
# progress.place(x=30, y=60, width=200)
progress = customtkinter.CTkProgressBar(root, orientation="horizontal",
	width=300,
	height=20,
	corner_radius=20,
	border_width=2,
	border_color="",
	fg_color="grey",
	progress_color="blue",
	mode="determinate",
	determinate_speed=1,
	indeterminate_speed=.5,
	)
progress.pack(pady=40)
progress.set(0)
# Function responsible for the updation 
# of the progress bar value 
def bar(): 
    progress.start()
    print("Authenticating Strava API...")
    root.after(1000, lambda: progress.set(25))
    root.update_idletasks() 
    # root.after(1000)
    print("New access token obtained and saved.")
    print("Uploading file xyz.fit to Strava...")
    root.after(1000, lambda: progress.set(50))
    root.update_idletasks() 
    # root.after(1000)
    print("File uploaded. Checking upload status...")
    root.after(1000, lambda: progress.set(75))
    root.update_idletasks() 
    # root.after(1000)
  
    root.after(1000, lambda: progress.set(85))
    root.update_idletasks() 
    # root.after(1000)
  
    root.after(1000, lambda: progress.set(95))
    root.update_idletasks() 
    # root.after(1000)
    root.after(1000, lambda: progress.set(100))
    print("Activity is ready!")
    progress.stop()
  
progress.pack(pady = 10) 
  
# This button will initialize 
# the progress bar 
Button(root, text = 'Start', command = bar).pack(pady = 10) 
  
# infinite loop 
root.mainloop() 