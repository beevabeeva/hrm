import ttkbootstrap as ttk
from ttkbootstrap.constants import *

def start_upload():
    print("Authenticating Strava API...")
    # progress.set(0.0)  # Ensure the progress starts at 0 visually
    # root.update_idletasks()  # Force an update to reflect the progress

    # Simulate the process with delays
    root.after(1000, lambda: progress.set(1))
    root.after(1000, lambda: progress.set(2))
    root.after(1000, lambda: progress.set(3))
    root.after(1000, lambda: progress.set(4))
    root.after(1000, lambda: progress.set(5))
    root.after(1000, lambda: progress.set(6))
    root.after(1000, lambda: progress.set(7))
    root.after(1000, lambda: progress.set(8))
    root.after(1000, lambda: progress.set(9))
    root.after(1000, lambda: progress.set(10))
    root.after(1000, lambda: progress.set(11))
    root.after(1000, lambda: progress.set(12))
    root.after(1000, lambda: progress.set(13))

    root.after(2000, lambda: print("New access token obtained and saved."))
    root.after(2000, lambda: progress.set(10))
    root.after(3000, lambda: print("Uploading file xyz.fit to Strava..."))
    root.after(3000, lambda: progress.set(55))
    root.after(3000, lambda: print("File uploaded. Checking upload status..."))
    root.after(4000, lambda: progress.set(70))
    root.after(4000, lambda: progress.set(99))
    root.after(6000, lambda: progress.set(100))
    root.after(4000, lambda: print("Activity is ready!"))

# Initialize the main window
root = ttk.Window(themename="superhero")
root.title("Progress Bar with ttkbootstrap")

# Set up the progress variable and progress bar
progress = ttk.DoubleVar(value=0.0)
s = ttk.Style()
s.configure("TProgressbar", thickness=50000)
progressbar = ttk.Progressbar(root, variable=progress, bootstyle="warning-striped", length=200)
progressbar.place(x=30, y=60)

# Start upload button
start_upload_button = ttk.Button(root, text="Start Upload", command=start_upload, bootstyle="primary")
start_upload_button.pack(pady=10)

# Set window size
root.geometry("300x200")

# Start the main loop
root.mainloop()
