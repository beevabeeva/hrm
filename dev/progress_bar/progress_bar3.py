import tkinter as tk
from tkinter import ttk



def start_upload():
    root.after(0, lambda: progress.set(0))
    root.update_idletasks() 
    print("Authenticating Strava API...")
    root.after(10, lambda: progress.set(25))
    root.update_idletasks() 
    # Simulate the process with delays
    root.after(2000, lambda: progress.set(25))
    root.update_idletasks() 
    root.after(2000, lambda: print("New access token obtained and saved."))
    root.update_idletasks() 
    # root.after(2000, lambda: progress.set(50))
    root.after(2000, lambda: print("Uploading file xyz.fit to Strava..."))
    root.update_idletasks() 
    root.after(2000, lambda: progress.set(55))
    root.update_idletasks() 
    root.after(2000, lambda: print("File uploaded. Checking upload status..."))
    root.update_idletasks() 
    root.after(2000, lambda: progress.set(60)) #attempt 1
    root.update_idletasks() 
    root.after(2000, lambda: progress.set(70)) #attempt 2
    root.update_idletasks() 
    root.after(2000, lambda: progress.set(80)) #attempt 3
    root.update_idletasks() 
    root.after(2000, lambda: progress.set(90)) #attempt 4
    root.update_idletasks() 
    root.after(2000, lambda: progress.set(100))
    root.update_idletasks() 
    root.after(2000, lambda: print("Activity is ready!"))
    root.update_idletasks() 

root = tk.Tk()
root.title("Progress Bar in Tk")

# Progress variable and progress bar setup
progress = tk.IntVar()
s = ttk.Style()
s.theme_use("default")
s.configure("TProgressbar", thickness=50)
progressbar = ttk.Progressbar(root, maximum=100, variable=progress,style="TProgressbar")
progressbar.place(x=30, y=60, width=200)
progressbar.pack()

# Start upload button
start_upload_button = tk.Button(root, text="Start Upload", command=start_upload)
start_upload_button.pack(pady=10)

root.geometry("300x200")
root.mainloop()
