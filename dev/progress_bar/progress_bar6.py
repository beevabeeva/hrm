import customtkinter as ctk
import time
import tkinter as tk
from tkinter import ttk

def start_upload():
    print(progress.get())
    print("Authenticating Strava API...")
    progress.set(0)
    
    # progress.start()
    # progress.set(0)
    # root.after(1000, lambda: progress.set(0.0))
    # Simulate the process with delays
    root.after(1000, lambda: progress.set(2))
    root.after(2000, lambda: print("New access token obtained and saved."))
    root.after(2000, lambda: progress.set(10))
    root.after(3000, lambda: print("Uploading file xyz.fit to Strava..."))
    root.after(3000, lambda: progress.set(55))
    root.after(3000, lambda: print("File uploaded. Checking upload status..."))
    root.after(4000, lambda: progress.set(70))
    root.after(4000, lambda: progress.set(99))
    root.after(6000, lambda: progress.set(100))
    root.after(4000, lambda: print("Activity is ready!"))

# Initialize the main window
root = ctk.CTk()
root.title("Progress Bar")

# Set up the progress variable and progress bar
# progress = ctk.DoubleVar()
progress = tk.IntVar()

# progressbar = ctk.CTkProgressBar(root, variable=progress, width=200,height=20)
s = ttk.Style()
s.theme_use("plastik")
s.configure("TProgressbar", thickness=500,foreground="black", background="white", border="", )
progressbar = ttk.Progressbar(root, maximum=100, variable=progress,style="TProgressbar",length=300)

progressbar.place(x=30, y=60)
# progressbar['value']=0

# Start upload button
start_upload_button = ctk.CTkButton(root, text="Start Upload", command=start_upload)
start_upload_button.pack(pady=10)

# Set window size
root.geometry("300x200")

# Start the main loop
root.mainloop()
