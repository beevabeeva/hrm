from tkinter import ttk
from tkinter import *
import time

root = Tk()
p = ttk.Progressbar(root, orient="horizontal", length=200, mode="determinate",
                    takefocus=True, maximum=100)
p['value'] = 0
p.pack()

def start():
    if p['value'] < 100:
        print(p['value'])
        if p['value'] == 25:
            print("New access token obtained and saved.")
            root.after(100, start)   
        elif p['value'] == 50:
            print("Uploading file xyz.fit to Strava...")
            time.sleep(5)
            root.after(100, start) 
        elif p['value'] == 70 | 75 | 80 | 85:
            print("File uploaded. Checking upload status...")
            root.after(100, start) 
        elif p['value'] ==99:
            p['value'] =100
            print("Activity is ready!")

        p['value'] += 5

        root.after(100, start)

root.after(1000, start)
root.mainloop()