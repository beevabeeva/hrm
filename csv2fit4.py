import csv
import datetime
from fit_tool.fit_file_builder import FitFileBuilder
from fit_tool.profile.messages.file_id_message import FileIdMessage
from fit_tool.profile.messages.record_message import RecordMessage
from fit_tool.profile.profile_type import Manufacturer, FileType


def convert_csv_to_fit(csv_file, fit_file):
    # Create the FileIdMessage
    file_id_message = FileIdMessage()
    file_id_message.type = FileType.ACTIVITY
    file_id_message.manufacturer = Manufacturer.DEVELOPMENT.value
    file_id_message.product = 0
    file_id_message.time_created = int(datetime.datetime.now().timestamp() * 1000)
    file_id_message.serial_number = 0x12345678

    # Create the FitFileBuilder
    builder = FitFileBuilder(auto_define=True)

    # Add the FileIdMessage to the FIT file
    builder.add(file_id_message)

    # Read the CSV file
    with open(csv_file, 'r') as file:
        reader = csv.reader(file)
        
        # Skip the first row (header)
        next(reader)
        
        # Process data rows
        for row in reader:
            if row[0] == "Data":  # Only process rows marked as "Data"
                # Initialize RecordMessage
                record_message = RecordMessage()
                
                # Loop through each `Field`, `Value`, `Unit` triplet
                for i in range(3, len(row), 3):  # Starting from 3rd column (index 3)
                    field = row[i]
                    value = row[i + 1]
                    unit = row[i + 2]

                    # Handle each specific field based on its name (e.g., "timestamp", "heart_rate")
                    if field == "timestamp":
                        record_message.timestamp = int(value) * 1000  # Convert to milliseconds
                    elif field == "heart_rate":
                        record_message.heart_rate = int(value)
                    elif field == "power":
                        record_message.power = int(value)
                    elif field == "cadence":
                        record_message.cadence = int(value)
                    elif field == "speed":
                        record_message.speed = float(value)
                    elif field == "distance":
                        record_message.distance = float(value)

                # Add the RecordMessage to the FIT file
                builder.add(record_message)

    # Build the FIT file
    fit_file_data = builder.build()

    # Convert FitFile object to bytes
    fit_file_bytes = fit_file_data.to_bytes()

    # Write the FIT file to disk
    with open(fit_file, 'wb') as f:
        f.write(fit_file_bytes)


# Example usage
convert_csv_to_fit('converted_output.csv', 'myconverter_output.fit')
