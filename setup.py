from setuptools import setup

setup(
    app=["hrm.py"],
    data_files=["user_age.txt"],
    setup_requires=["py2app"],
)