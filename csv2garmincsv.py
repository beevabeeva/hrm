import csv
import time

def convert_to_fit_csv(input_file, output_file):
    # Garmin SDK requires timestamps in Unix epoch time format
    def convert_to_unix_time(timestamp_str):
        time_struct = time.strptime(timestamp_str, "%Y-%m-%d %H:%M:%S")
        return int(time.mktime(time_struct))
    
    # Prepare the output file
    with open(input_file, 'r') as infile, open(output_file, 'w', newline='') as outfile:
        reader = csv.DictReader(infile)
        writer = csv.writer(outfile)

        # Write the Garmin SDK-compatible header
        writer.writerow([
            "Type", "Local Number", "Message", 
            "Field 1", "Value 1", "Units 1", 
            "Field 2", "Value 2", "Units 2", 
            "Field 3", "Value 3", "Units 3", 
            "Field 4", "Value 4", "Units 4",
            "Field 5", "Value 5", "Units 5"
        ])

        # Write the Definition row for "record" messages
        writer.writerow([
            "Definition", 0, "record",
            "timestamp", 1, "s",
            "heart_rate", 1, "bpm",
            "power", 1, "watts",
            "cadence", 1, "rpm",
            "speed", 1, "m/s",
            "distance", 1, "m"
        ])

        # Process each row in the input CSV
        for row in reader:
            # Convert timestamp to Unix time
            unix_time = convert_to_unix_time(row["timestamp"])

            # Write the Data row
            writer.writerow([
                "Data", 0, "record",
                "timestamp", unix_time, "s",
                "heart_rate", row["heart_rate"], "bpm",
                "power", row["power"], "watts",
                "cadence", row["cadence"], "rpm",
                "speed", row["speed"], "m/s",
                "distance", row["distance"], "m"
            ])

    print(f"Conversion completed. Output saved to {output_file}")

# Example usage
input_csv = "workouts/csv/workout_20241210_162912.csv"  # Replace with your input file
output_csv = "converted_output.csv"  # Replace with your desired output file
convert_to_fit_csv(input_csv, output_csv)
