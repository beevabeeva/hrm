import tkinter as tk

def update_font_size(value):
    """Update the font size of the label based on the slider value."""
    new_font_size = int(value)
    label.config(font=("Arial", new_font_size))

def toggle_slider(event):
    """Toggle the visibility of the slider when the window is clicked."""
    if slider.winfo_ismapped():
        slider.place_forget()
    else:
        slider.place(x=20, y=20)  # Adjust the position of the slider as needed
        slider.focus_set()  # Set focus to the slider for immediate interaction

def hide_slider(event):
    """Hide the slider if clicked outside of it."""
    if not slider.winfo_containing(event.x_root, event.y_root):
        slider.place_forget()

def adjust_slider_range(event=None):
    """Adjust the slider's range based on the window size."""
    # Get the current window size
    width = window.winfo_width()
    height = window.winfo_height()

    # Set new min and max values for the slider based on window dimensions
    min_font_size = 10
    max_font_size = max(width, height)  # Example calculation for max size

    slider.config(from_=min_font_size, to=max_font_size)

# Create the main window
window = tk.Tk()
window.title("Font Size Adjuster")
window.geometry("400x300")

# Create the label
label = tk.Label(window, text="Sample Text", font=("Arial", 20))
label.pack(expand=True)

# Create the slider
slider = tk.Scale(window, from_=10, to=100, orient="horizontal", command=update_font_size, showvalue=0)
slider.place_forget()  # Initially hide the slider

# Bind left-click event to toggle the slider visibility
window.bind("<Button-2>", toggle_slider)

# Bind mouse motion to hide the slider if clicked outside
window.bind("<Motion>", hide_slider) #hmm not sure I like this

# Bind window resize event to adjust slider range
window.bind("<Configure>", adjust_slider_range)

window.mainloop()
