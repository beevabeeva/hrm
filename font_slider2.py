import tkinter as tk
import customtkinter as ctk

def show_slider(event):
    global slider_window, slider
    if slider_window and slider_window.winfo_ismapped():
        slider_window.withdraw()
        return

    # Create a new transparent window
    slider_window = tk.Toplevel()
    slider_window.wm_overrideredirect(True)
    slider_window.attributes('-topmost', True)
    slider_window.attributes('-transparent', True)

    # Position the slider window next to the clicked window
    x = window.winfo_rootx() + event.x_root + 20
    y = window.winfo_rooty() + event.y_root

    slider_window.geometry(f'200x50+{x}+{y}')

    # Create a CTkSlider
    slider = ctk.CTkSlider(slider_window, from_=10, to=100, command=adjust_font_size)
    slider.pack(pady=10, padx=10)

    # Adjust the slider range based on the window size
    update_slider_range()

def adjust_font_size(value):
    new_size = int(float(value))
    label.config(font=("Arial", new_size))
    
def update_slider_range():
    global slider
    new_max = min(window.winfo_width(), window.winfo_height()) // 10
    slider.configure(from_=10, to=new_max)

def hide_slider(event):
    if slider_window and slider_window.winfo_ismapped():
        # Check if click is inside the main window
        if not (window.winfo_rootx() <= event.x_root <= window.winfo_rootx() + window.winfo_width() and
                window.winfo_rooty() <= event.y_root <= window.winfo_rooty() + window.winfo_height()):
            slider_window.withdraw()

# Initialize the main window
window = tk.Tk()
window.geometry('300x200')
window.title('Main Window')

label = tk.Label(window, text="Sample Text", font=("Arial", 20))
label.pack(expand=True, fill=tk.BOTH)

# Initialize slider window
slider_window = None

# Bind click events to show and hide slider
window.bind('<Button-1>', show_slider)  # Show slider on left click
window.bind('<Button-2>', hide_slider)  # Hide slider on left click

window.mainloop()
