import csv
import datetime
from fit_tool.fit_file_builder import FitFileBuilder
from fit_tool.profile.messages.file_id_message import FileIdMessage
from fit_tool.profile.messages.record_message import RecordMessage
from fit_tool.profile.profile_type import Manufacturer, FileType


def convert_csv_to_fit(csv_file, fit_file):
    FIT_OFFSET_SECONDS = 631065600 # NOTE!! STRAVA APPLIES THIS OFFSET ON THEIR SIDE - SO DON"T APPLY IT HERE (figured out through trial and error - not in docs!)

    # Create the FileIdMessage
    file_id_message = FileIdMessage()
    file_id_message.type = FileType.ACTIVITY
    file_id_message.manufacturer = Manufacturer.DEVELOPMENT.value
    file_id_message.product = 0
    file_id_message.time_created = round((datetime.datetime.now().timestamp()-631065600) * 1000)
    file_id_message.serial_number = 0x12345678

    # Create the FitFileBuilder
    builder = FitFileBuilder(auto_define=True)

    # Add the FileIdMessage to the FIT file
    builder.add(file_id_message)

    # fit_epoch = datetime.datetime(1989, 12, 31, 0, 0, 0)
    # Define the offset between Unix Epoch and FIT Epoch
   

    # Read the CSV file and add records to the FIT file
    with open(csv_file, 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            original_timestamp = datetime.datetime.strptime(row['timestamp'], '%Y-%m-%d %H:%M:%S')
            timestamp_unix = int(original_timestamp.timestamp())
            fit_timestamp = abs(timestamp_unix)# - FIT_OFFSET_SECONDS) #hack to make Strava not see the activity 20 years behind. -works!
            print(f"Converted FIT timestamp: {fit_timestamp}")   
            # Create a RecordMessage for each row
            record_message = RecordMessage()
            record_message.timestamp = fit_timestamp*1000
            print("record_message.timestamp ")
            print(record_message.timestamp)
            record_message.heart_rate = int(row['heart_rate'])
            record_message.power = int(row['power'])
            record_message.cadence = int(row['cadence'])
            record_message.speed = float(row['speed'])
            record_message.distance = float(row['distance'])
            # Add the RecordMessage to the FIT file
            builder.add(record_message)
            print("builder.add(record_message)")
            print(record_message)

    # Build the FIT file
    fit_file_data = builder.build()
    print("fit_file_data = builder.build()")
    print(fit_file_data)
    # Convert FitFile object to bytes
    fit_file_bytes = fit_file_data.to_bytes()

    # Write the FIT file to disk
    with open(fit_file, 'wb') as f:
        f.write(fit_file_bytes)

# Example usage
convert_csv_to_fit('converted_output.csv', 'myconverter_output.fit')

